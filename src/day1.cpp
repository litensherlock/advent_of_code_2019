#include <gtest/gtest.h>

#include <array>
#include <cstdio>

constexpr long needed_fuel(const long mass) {
  return (mass / 3) - 2;
}

constexpr std::array module_masses{144475, 145308, 100615, 56900, 128773, 65519, 74165, 99081, 141047, 149128, 148282, 109528, 55909, 70885, 115049, 149631, 52276, 101944, 113005, 102876, 64365, 71178, 122767, 86272, 139199, 78631, 71958, 81288, 70401, 77582, 118275, 115648, 91350, 121735, 130339, 55146, 137351, 101940, 112657, 133288, 81503, 136812, 67015, 142573, 125537, 99231, 61693, 85719, 80659, 148431, 101176, 77853, 108201, 138945, 81804, 55795, 141837, 113490, 57932, 81023, 76756, 79023, 73527, 75874, 63332, 62055, 76124, 54254, 68482, 141113, 84335, 58747, 84723, 137564, 132605, 94970, 50312, 89127, 143858, 124587, 52272, 138039, 53782, 93085, 83456, 94432, 121481, 93700, 114222, 117849, 147460, 110324, 75337, 130464, 88805, 109489, 71109, 95625, 115832, 123252};

TEST(advent_of_code, day_1_task_1) {
  long sum = 0;
  for (auto mass : module_masses) {
    sum += needed_fuel(mass);
  }
  printf("%zd\n", sum);
  EXPECT_EQ(3297896, sum);
}

TEST(advent_of_code, day_1_task_2_test) {
  long total_fuel = 0;
  const long fuel_for_module = needed_fuel(14);
  total_fuel += fuel_for_module;

  long new_fuel = needed_fuel(fuel_for_module);
  while (new_fuel > 0) {
    total_fuel += new_fuel;
    new_fuel = needed_fuel(new_fuel);
  }
  EXPECT_EQ(2, total_fuel);
}

TEST(advent_of_code, day_1_task_2_test2) {
  long total_fuel = 0;
  const long fuel_for_module = needed_fuel(1969);
  total_fuel += fuel_for_module;

  long new_fuel = needed_fuel(fuel_for_module);
  while (new_fuel > 0) {
    total_fuel += new_fuel;
    new_fuel = needed_fuel(new_fuel);
  }
  EXPECT_EQ(966, total_fuel);
}

TEST(advent_of_code, day_1_task_2_test3) {
  long total_fuel = 0;
  const long fuel_for_module = needed_fuel(100756);
  total_fuel += fuel_for_module;

  long new_fuel = needed_fuel(fuel_for_module);
  while (new_fuel > 0) {
    total_fuel += new_fuel;
    new_fuel = needed_fuel(new_fuel);
  }
  EXPECT_EQ(50346, total_fuel);
}

TEST(advent_of_code, day_1_task_2) {
  long total_fuel = 0;
  for (auto mass : module_masses) {
    const long fuel_for_module = needed_fuel(mass);
    total_fuel += fuel_for_module;

    long new_fuel = needed_fuel(fuel_for_module);
    while (new_fuel > 0) {
      total_fuel += new_fuel;
      new_fuel = needed_fuel(new_fuel);
    }
  }
  printf("%zd\n", total_fuel);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}