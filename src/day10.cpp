#include <gtest/gtest.h>

#include <vector>
#include <string>
#include <cmath>

auto input = "..............#.#...............#....#....\n"
             "#.##.......#....#.#..##........#...#......\n"
             "..#.....#....#..#.#....#.....#.#.##..#..#.\n"
             "...........##...#...##....#.#.#....#.##..#\n"
             "....##....#...........#..#....#......#.###\n"
             ".#...#......#.#.#.#...#....#.##.##......##\n"
             "#.##....#.....#.....#...####........###...\n"
             ".####....#.......#...##..#..#......#...#..\n"
             "...............#...........#..#.#.#.......\n"
             "........#.........##...#..........#..##...\n"
             "...#..................#....#....##..#.....\n"
             ".............#..#.#.........#........#.##.\n"
             "...#.#....................##..##..........\n"
             ".....#.#...##..............#...........#..\n"
             "......#..###.#........#.....#.##.#......#.\n"
             "#......#.#.....#...........##.#.....#..#.#\n"
             ".#.............#..#.....##.....###..#..#..\n"
             ".#...#.....#.....##.#......##....##....#..\n"
             ".........#.#..##............#..#...#......\n"
             "..#..##...#.#..#....#..#.#.......#.##.....\n"
             "#.......#.#....#.#..##.#...#.......#..###.\n"
             ".#..........#...##.#....#...#.#.........#.\n"
             "..#.#.......##..#.##..#.......#.###.......\n"
             "...#....###...#......#..#.....####........\n"
             ".............#.#..........#....#......#...\n"
             "#................#..................#.###.\n"
             "..###.........##...##..##.................\n"
             ".#.........#.#####..#...##....#...##......\n"
             "........#.#...#......#.................##.\n"
             ".##.....#..##.##.#....#....#......#.#....#\n"
             ".....#...........#.............#.....#....\n"
             "........#.##.#...#.###.###....#.#......#..\n"
             "..#...#.......###..#...#.##.....###.....#.\n"
             "....#.....#..#.....#...#......###...###...\n"
             "#..##.###...##.....#.....#....#...###..#..\n"
             "........######.#...............#...#.#...#\n"
             "...#.....####.##.....##...##..............\n"
             "###..#......#...............#......#...#..\n"
             "#..#...#.#........#.#.#...#..#....#.#.####\n"
             "#..#...#..........##.#.....##........#.#..\n"
             "........#....#..###..##....#.#.......##..#\n"
             ".................##............#.......#..";

struct vector {
  vector operator/(const int rhs) {
    return {x/rhs, y/rhs};
  }

  vector& operator/=(const int rhs) {
    x/=rhs;
    y/=rhs;
    return *this;
  }

  int x;
  int y;
};

struct Point {
  Point() = default;
  Point(int x, int y, double angle) : x(x), y(y), angle(angle) {}
  Point(int x, int y) : x(x), y(y) {}

  Point& operator+=(const vector & rhs) noexcept {
    x += rhs.x;
    y += rhs.y;
    return *this;
  }
  
  bool operator==(const Point &rhs) const noexcept {
    return x == rhs.x &&
           y == rhs.y;
  }

  bool operator!=(const Point &rhs) const noexcept {
    return !(rhs == *this);
  }

  vector operator-(const Point &rhs) const noexcept {
    return vector{this->x - rhs.x, this->y - rhs.y};
  }

  int x{};
  int y{};
  double angle{};
};

std::vector<std::vector<int>> parse(const std::string & text) {
  std::vector<std::vector<int>> res;
  std::stringstream ss(text);
  for (std::string line; std::getline(ss, line);) {
    std::vector<int> & numberLine = res.emplace_back(line.size(), -1);
    for (int i = 0; i < static_cast<int>(line.size()); ++i) {
      if (line.at(i) == '#') {
        numberLine[i] = 0;
      }
    }
  }
  return res;
}

template <typename Number>
Number gcd(Number u, Number v) {
  while (v != 0) {
    Number r = u % v;
    u = v;
    v = r;
  }
  return u;
}

namespace detail {
template<typename Pred>
static bool in_line_of_sight(Point a, Point b, const std::vector<std::vector<int>> &in_sight_map, Pred stop_at) noexcept(noexcept(stop_at(4))) {
  vector pv = b - a;
  pv /= gcd(std::abs(pv.x), std::abs(pv.y));
  for (a += pv; a != b; a += pv) {
    if (stop_at(in_sight_map[a.y][a.x])) {
      return false;
    }
  }
  return true;
}
} // detail

bool in_line_of_sight(Point a, Point b, const std::vector<std::vector<int>> & in_sight_map) {
  return detail::in_line_of_sight(a, b, in_sight_map, [](int i){return i != -1;});
}

bool in_line_of_death(Point a, Point b, const std::vector<std::vector<int>> & in_sight_map) {
  return detail::in_line_of_sight(a, b, in_sight_map, [](int i){return i == 0;});
}

void populate_sight_map(std::vector<std::vector<int>> & in_sight_map) {
  for (int row = 0; row < static_cast<int>(in_sight_map.size()); ++row) {
    std::vector<int> &line = in_sight_map[row];
    for (int column = 0; column < static_cast<int>(line.size()); ++column) {
      if (line[column] == -1)
        continue;

      Point candidate = {column, row};

      // find all points in line of sight of the candidate position
      for (int y = 0; y < static_cast<int>(in_sight_map.size()); ++y) {
        std::vector<int> &l = in_sight_map[y];
        for (int x = 0; x < static_cast<int>(l.size()); ++x) {
          // skip empty spaces, and skip the space of the candidate
          if ((l[x] == -1) || (x == candidate.x && y == candidate.y))
            continue;

          Point p = {x, y};
          if (in_line_of_sight(candidate, p, in_sight_map))
            ++l[x];
        }
      }
    }
  }
}

int count_asteroids(const std::vector<std::vector<int>> & map) {
  int sum = 0;
  for (const auto &item : map) {
    sum += std::count(item.begin(), item.end(), 0);
  }
  return sum;
}


namespace detail {
static double get_angle_k1(vector v) {
  // + PI to move it from straight down to straight up, + PI/2 to move it into the first (rotated) quadrant
  return M_PI + M_PI_2 + std::atan(static_cast<double>(v.y) / static_cast<double>(v.x));
}

static double get_angle_k2(vector v) {
  // 0 is straight down, angel increases towards straight to the left
  return M_PI_2 - std::atan(static_cast<double>(v.y) / static_cast<double>(-v.x));
}
static double get_angle_k3(vector v) {
  // 0 is to the left, offset with PI/2 and use the angle to move upwards
  return M_PI_2 - std::atan(static_cast<double>(v.y) / static_cast<double>(-v.x));
}
static double get_angle_k4(vector v) {
  // + PI to move it from straight down to straight up, + PI/2 to move it to point to the right, 0 is straight to the right
  return M_PI + M_PI_2 - std::atan(static_cast<double>(v.y) / static_cast<double>(-v.x));
}

// Q1 is down left, Q2 is down right, Q3 is up left, Q4 is up right
static double get_angle(vector v) {
  double angle;
  if (v.x > 0 && v.y > 0) {
    angle = detail::get_angle_k1(v);
  } else if (v.x <= 0 && v.y >= 0) {
    angle = detail::get_angle_k2(v);
  } else if (v.x <= 0 && v.y <= 0) {
    angle = detail::get_angle_k3(v);
  } else { // positive x, negative y,
    angle = detail::get_angle_k4(v);
  }
  return angle;
}
}

static double get_angle(const Point from, const Point to) {
  const vector v = to - from;
  double angle;
  angle = detail::get_angle(v);
  return angle;
}

static void get_points(const Point &station, const std::vector<std::vector<int>> &map, std::vector<Point> &points) {
  for (int y = 0; y < static_cast<int>(map.size()); ++y) {
    const std::vector<int> &l = map[y];
    for (int x = 0; x < static_cast<int>(l.size()); ++x) {
      // skip empty spaces, skip already destroyed asteroids, and skip the space of the candidate
      if ((l[x] != 0) || (x == station.x && y == station.y))
        continue;

      Point p = {x, y};
      if (in_line_of_death(station, p, map))
        points.emplace_back(x, y, get_angle(station, p));
    }
  }

  // the angels has 0 straight down, rotate so that 0 is straight up to make the sorting correct
  std::for_each(points.begin(), points.end(),
      [](Point & a) noexcept {
        a.angle += M_PI;
        if (a.angle >= M_PI*2) {
          a.angle -= M_PI*2;
        }
      });
  std::sort(points.begin(), points.end(),
      [](const Point & a, const Point & b) noexcept {return a.angle < b.angle;});
}

void populate_kill_map(Point station, std::vector<std::vector<int>> & map) {
  int counter = 1;
  int asteroids_left = count_asteroids(map);

  std::vector<Point> points;
  points.reserve(map.size()*map.at(0).size());

  // find all points in line of sight of the candidate position
  while(asteroids_left - counter > 0) {
    get_points(station, map, points);
    for (const auto &point : points) {
      if (in_line_of_death(station, point, map)) {
        map[point.y][point.x] = counter++;
      }
    }
    points.clear();
  }
}

Point get_best_location(const std::vector<std::vector<int>> & in_sight_map) noexcept {
  Point best_p{};
  int max = std::numeric_limits<int>::min();
  for (int row = 0; row < static_cast<int>(in_sight_map.size()); ++row) {
    const std::vector<int> &line = in_sight_map[row];
    for (int column = 0; column < static_cast<int>(line.size()); ++column) {
      if (max < line[column]) {
        max = line[column];
        best_p = {column, row};
      }
    }
  }
  return best_p;
}

Point get_nth_kill(const int n, const std::vector<std::vector<int>> & in_sight_map) noexcept {
  for (int row = 0; row < static_cast<int>(in_sight_map.size()); ++row) {
    const std::vector<int> &line = in_sight_map[row];
    for (int column = 0; column < static_cast<int>(line.size()); ++column) {
      if (line[column] == n) {
        return {column, row};
      }
    }
  }
  return {-1, -1};
}

void print_map(const std::vector<std::vector<int>> map) {
  std::cout << '\n';
  for (const auto &row : map) {
    for (const auto &point : row) {
      std::cout << std::setw(2) << point << ' ';
    }
    std::cout << '\n';
  }
}

TEST(advent_of_code, day_10_test) {
  auto test_input = ".#..#\n"
                    ".....\n"
                    "#####\n"
                    "....#\n"
                    "...##";
  const std::vector<std::vector<int>> expected{{-1, 0, -1, -1, 0},
                                               {-1, -1, -1, -1, -1},
                                               {0, 0, 0, 0, 0},
                                               {-1, -1, -1, -1, 0},
                                               {-1, -1, -1, 0, 0}};
  const std::vector<std::vector<int>> value = parse(test_input);
  EXPECT_EQ(expected, value);
}

TEST(advent_of_code, day_10_test_2) {
  const std::vector<std::vector<int>> test_map{{0, 0, -1},
                                               {-1, -1, -1},
                                               {0, 0, 0}};
  bool in_sight = in_line_of_sight({2, 2}, {0, 0}, test_map);
  EXPECT_EQ(true, in_sight);

  in_sight = in_line_of_sight({2, 2}, {0, 2}, test_map);
  EXPECT_EQ(false, in_sight);

  in_sight = in_line_of_sight({2, 2}, {1, 2}, test_map);
  EXPECT_EQ(true, in_sight);

  in_sight = in_line_of_sight({2, 2}, {1, 0}, test_map);
  EXPECT_EQ(true, in_sight);
}


TEST(advent_of_code, day_10_test_3) {
  const std::vector<std::vector<int>> test_map{{0, 0, -1},
                                               {-1, -1, -1},
                                               {0, 0, 0}};
  bool in_sight = in_line_of_sight({1, 0}, {0, 0}, test_map);
  EXPECT_EQ(true, in_sight);

  in_sight = in_line_of_sight({1, 0}, {0, 2}, test_map);
  EXPECT_EQ(true, in_sight);

  in_sight = in_line_of_sight({1, 0}, {1, 2}, test_map);
  EXPECT_EQ(true, in_sight);

  in_sight = in_line_of_sight({1, 0}, {2, 2}, test_map);
  EXPECT_EQ(true, in_sight);
}


TEST(advent_of_code, day_10_test_4) {
  const std::vector<std::vector<int>> expected{{-1, 7, -1, -1, 7},
                                               {-1, -1, -1, -1, -1},
                                               {6, 7, 7, 7, 5},
                                               {-1, -1, -1, -1, 7},
                                               {-1, -1, -1, 8, 7}};
  auto test_input = ".#..#\n"
                    ".....\n"
                    "#####\n"
                    "....#\n"
                    "...##";
  std::vector<std::vector<int>> map = parse(test_input);
  populate_sight_map(map);
  EXPECT_EQ(expected, map);
}


TEST(advent_of_code, day_10_test_5) {
  const Point expected{3, 4};
  auto test_input = ".#..#\n"
                    ".....\n"
                    "#####\n"
                    "....#\n"
                    "...##";
  std::vector<std::vector<int>> map = parse(test_input);
  populate_sight_map(map);
  const Point location = get_best_location(map);
  EXPECT_EQ(expected, location);
}

TEST(advent_of_code, day_10_test_6) {
  const Point expected{5,8};
  auto test_input = "......#.#.\n"
                    "#..#.#....\n"
                    "..#######.\n"
                    ".#.#.###..\n"
                    ".#..#.....\n"
                    "..#....#.#\n"
                    "#..#....#.\n"
                    ".##.#..###\n"
                    "##...#..#.\n"
                    ".#....####";
  std::vector<std::vector<int>> map = parse(test_input);
  populate_sight_map(map);
  const Point location = get_best_location(map);
  EXPECT_EQ(expected, location);
}

TEST(advent_of_code, day_10_test_7) {
  const Point expected{1,2};
  auto test_input = "#.#...#.#.\n"
                    ".###....#.\n"
                    ".#....#...\n"
                    "##.#.#.#.#\n"
                    "....#.#.#.\n"
                    ".##..###.#\n"
                    "..#...##..\n"
                    "..##....##\n"
                    "......#...\n"
                    ".####.###.";
  std::vector<std::vector<int>> map = parse(test_input);
  populate_sight_map(map);
  const Point location = get_best_location(map);
  EXPECT_EQ(expected, location);
}


TEST(advent_of_code, day_10_test_8) {
  const Point expected{6,3};
  auto test_input = ".#..#..###\n"
                    "####.###.#\n"
                    "....###.#.\n"
                    "..###.##.#\n"
                    "##.##.#.#.\n"
                    "....###..#\n"
                    "..#.#..#.#\n"
                    "#..#.#.###\n"
                    ".##...##.#\n"
                    ".....#.#..";
  std::vector<std::vector<int>> map = parse(test_input);
  populate_sight_map(map);
  const Point location = get_best_location(map);
  EXPECT_EQ(expected, location);
}

TEST(advent_of_code, day_10_test_9) {
  const Point expected{11,13};
  auto test_input = ".#..##.###...#######\n"
                    "##.############..##.\n"
                    ".#.######.########.#\n"
                    ".###.#######.####.#.\n"
                    "#####.##.#.##.###.##\n"
                    "..#####..#.#########\n"
                    "####################\n"
                    "#.####....###.#.#.##\n"
                    "##.#################\n"
                    "#####.##.###..####..\n"
                    "..######..##.#######\n"
                    "####.##.####...##..#\n"
                    ".#####..#.######.###\n"
                    "##...#.##########...\n"
                    "#.##########.#######\n"
                    ".####.#.###.###.#.##\n"
                    "....##.##.###..#####\n"
                    ".#.#.###########.###\n"
                    "#.#.#.#####.####.###\n"
                    "###.##.####.##.#..##";
  std::vector<std::vector<int>> map = parse(test_input);
  populate_sight_map(map);
  const Point location = get_best_location(map);
  EXPECT_EQ(expected, location);
}


TEST(advent_of_code, kill_map_test_1) {
  const Point station = {0, 2};
  std::vector<std::vector<int>> test_map{{0, 0, -1},
                                         {-1, -1, -1},
                                         {0, 0, 0}};

  const std::vector<std::vector<int>> expected{{1, 2, -1},
                                               {-1, -1, -1},
                                               {0, 3, 4}};

  populate_kill_map(station, test_map);
  EXPECT_EQ(expected, test_map);
}

TEST(advent_of_code, kill_map_test_2) {
  const Point station = {1, 1};
  std::vector<std::vector<int>> test_map{{-1, 0,  0},
                                         { 0, 0, -1},
                                         { 0, 0,  0}};

  const std::vector<std::vector<int>> expected{{-1, 1, 2},
                                               {6, 0, -1},
                                               {5, 4, 3}};

  populate_kill_map(station, test_map);
  EXPECT_EQ(expected, test_map);
}


TEST(advent_of_code, kill_map_test_3) {
  const Point station = {1, 2};
  std::vector<std::vector<int>> test_map{{-1, -1,  0,  0},
                                         { 0,  0,  0, -1},
                                         {-1,  0,  0, -1},
                                         {-1,  0, -1,  0}};

  const std::vector<std::vector<int>> expected{{-1, -1,  2,  8},
                                               { 7,  1,  3, -1},
                                               {-1,  0,  4, -1},
                                               {-1,  6, -1,  5}};

  populate_kill_map(station, test_map);
  EXPECT_EQ(expected, test_map);
}

TEST(advent_of_code, kill_map_test_4) {
  const Point station = {2, 1};
  std::vector<std::vector<int>> test_map{{-1, -1,  0,  0},
                                         { 0,  0,  0, -1},
                                         {-1,  0,  0, -1},
                                         {-1,  0, -1,  0}};

  const std::vector<std::vector<int>> expected{{-1, -1,  1,  2},
                                               { 8,  7,  0, -1},
                                               {-1,  6,  4, -1},
                                               {-1,  5, -1,  3}};

  populate_kill_map(station, test_map);
  EXPECT_EQ(expected, test_map);
}

TEST(advent_of_code, kill_map_test_5) {
  const Point station = {0, 0};
  std::vector<std::vector<int>> test_map{{ 0, -1, -1, -1,  0,  0,  0, -1, -1},
                                         {-1, -1,  0, -1, -1, -1, -1,  0,  0}};

  const std::vector<std::vector<int>> expected{{ 0, -1, -1, -1, 1, 5, 6, -1, -1},
                                               {-1, -1, 4, -1, -1, -1, -1, 3, 2}};

  populate_kill_map(station, test_map);
  print_map(test_map);
  EXPECT_EQ(expected, test_map);
}

TEST(advent_of_code, kill_map_test_6) {
  const Point station = {8, 1};
  std::vector<std::vector<int>> test_map{{ 0, -1, -1, -1,  0,  0,  0, -1, -1},
                                         {-1, -1,  0, -1, -1, -1, -1,  0,  0}};

  const std::vector<std::vector<int>> expected{{ 2, -1, -1, -1,  3,  4,  5, -1, -1},
                                               {-1, -1,  6, -1, -1, -1, -1,  1,  0}};

  populate_kill_map(station, test_map);
  print_map(test_map);
  EXPECT_EQ(expected, test_map);
}

TEST(advent_of_code, day_10_task_3_test) {
  /**
   * 0 0 0
   * 0 S 0
   * 0 T 0
   */
  const double expected = 0;
  const Point station = {1, 1};
  const Point target = {1, 2};
  const double angle = get_angle(station, target);
  EXPECT_EQ(expected, angle);
}

TEST(advent_of_code, day_10_task_4_test) {
  /**
   * 0 0 0
   * 0 S 0
   * T 0 0
   */
  const double expected = M_PI_4;
  const Point station = {1, 1};
  const Point target = {0, 2};
  const double angle = get_angle(station, target);
  EXPECT_EQ(expected, angle);
}

TEST(advent_of_code, day_10_task_5_test) {
  /**
   * 0 0 0
   * T S 0
   * 0 0 0
   */
  const double expected = M_PI_2;
  const Point station = {1, 1};
  const Point target = {0, 1};
  const double angle = get_angle(station, target);
  EXPECT_EQ(expected, angle);
}

TEST(advent_of_code, day_10_task_6_test) {
  /**
   * T 0 0
   * 0 S 0
   * 0 0 0
   */
  const double expected = M_PI_4 * 3;
  const Point station = {1, 1};
  const Point target = {0, 0};
  const double angle = get_angle(station, target);
  EXPECT_EQ(expected, angle);
}

TEST(advent_of_code, day_10_task_7_test) {
  /**
   * 0 T 0
   * 0 S 0
   * 0 0 0
   */
  const double expected = M_PI;
  const Point station = {1, 1};
  const Point target = {1, 0};
  const double angle = get_angle(station, target);
  EXPECT_EQ(expected, angle);
}

TEST(advent_of_code, day_10_task_8_test) {
  /**
   * 0 0 T
   * 0 S 0
   * 0 0 0
   */
  const double expected = M_PI + M_PI_4;
  const Point station = {1, 1};
  const Point target = {2, 0};
  const double angle = get_angle(station, target);
  EXPECT_EQ(expected, angle);
}

TEST(advent_of_code, day_10_task_9_test) {
  /**
   * 0 0 0
   * 0 S T
   * 0 0 0
   */
  const double expected = M_PI + M_PI_2;
  const Point station = {1, 1};
  const Point target = {2, 1};
  const double angle = get_angle(station, target);
  EXPECT_EQ(expected, angle);
}

TEST(advent_of_code, day_10_task_10_test) {
  /**
   * 0 0 0
   * 0 S 0
   * 0 0 T
   */
  const double expected = M_PI + (M_PI_4 * 3);
  const Point station = {1, 1};
  const Point target = {2, 2};
  const double angle = get_angle(station, target);
  EXPECT_EQ(expected, angle);
}

TEST(advent_of_code, day_10_task_2_test_2) {
  const Point expected{8,2};
  const Point station{11,13};
  auto test_input = ".#..##.###...#######\n"
                    "##.############..##.\n"
                    ".#.######.########.#\n"
                    ".###.#######.####.#.\n"
                    "#####.##.#.##.###.##\n"
                    "..#####..#.#########\n"
                    "####################\n"
                    "#.####....###.#.#.##\n"
                    "##.#################\n"
                    "#####.##.###..####..\n"
                    "..######..##.#######\n"
                    "####.##.####...##..#\n"
                    ".#####..#.######.###\n"
                    "##...#.##########...\n"
                    "#.##########.#######\n"
                    ".####.#.###.###.#.##\n"
                    "....##.##.###..#####\n"
                    ".#.#.###########.###\n"
                    "#.#.#.#####.####.###\n"
                    "###.##.####.##.#..##";
  std::vector<std::vector<int>> map = parse(test_input);
  populate_kill_map(station, map);
  const Point location = get_nth_kill(200, map);
  EXPECT_EQ(expected, location);
}

TEST(advent_of_code, day_10_task_2_test_3) {
  const Point expected{8,1};
  const Point station{8,3};
  auto test_input = ".#....#####...#..\n"
                    "##...##.#####..##\n"
                    "##...#...#.#####.\n"
                    "..#.....#...###..\n"
                    "..#.#.....#....##";
  std::vector<std::vector<int>> map = parse(test_input);
  populate_kill_map(station, map);
  const Point location = get_nth_kill(1, map);
  EXPECT_EQ(expected, location);
}

TEST(advent_of_code, day_10_task_2_test_4) {
  const std::vector<std::vector<int>> expected{{ -1, 26, -1, -1, -1, -1, 29, 30, 31,  2,  4, -1, -1, -1, 33, -1, -1 },
                                               { 23, 24, -1, -1, -1, 27, 28, -1,  1,  3, 32,  6, 7, -1, -1, 9, 34 },
                                               { 21, 22, -1, -1, -1, 25, -1, -1, -1,  5, -1, 8, 10, 11, 12, 13, -1 },
                                               { -1, -1,  20, -1, -1, -1, -1, -1,  0, -1, -1, -1, 14, 35, 36, -1, -1 },
                                               { -1, -1,  19, -1,  18, -1, -1, -1, -1, -1, 17, -1, -1, -1, -1, 16, 15 }};
  const Point station{8,3};
  auto test_input = ".#....#####...#..\n"
                    "##...##.#####..##\n"
                    "##...#...#.#####.\n"
                    "..#.....#...###..\n"
                    "..#.#.....#....##";
  std::vector<std::vector<int>> map = parse(test_input);
  populate_kill_map(station, map);
  EXPECT_EQ(expected, map);
}


TEST(advent_of_code, day_10_task_2_test_5) {
  const Point station = {1, 2};
  std::vector<std::vector<int>> test_map{{0, 0, -1},
                                         {-1, -1, -1},
                                         {0, 0, 0}};

  const std::vector<std::vector<int>> expected{{4, 1, -1},
                                               {-1, -1, -1},
                                               {3, 0, 2}};

  populate_kill_map(station, test_map);
  EXPECT_EQ(expected, test_map);
}


TEST(advent_of_code, day_10) {
  const int expected = 347;
  std::vector<std::vector<int>> map = parse(input);
  populate_sight_map(map);
  const Point location = get_best_location(map);
  const int number_of_asteroids_in_sight = map[location.y][location.x];
  EXPECT_EQ(expected, number_of_asteroids_in_sight);
}

TEST(advent_of_code, day_10_task_2) {
  const Point expected{8,29};
  const Point station{26,36};
  std::vector<std::vector<int>> map = parse(input);
  populate_kill_map(station, map);
  const Point location = get_nth_kill(200, map);
  EXPECT_EQ(expected, location);
}


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}