#include <gtest/gtest.h>

#include <vector>
#include <unordered_map>
#include "intcode-computer.h"

static std::vector<long> input{3,8,1005,8,318,1106,0,11,0,0,0,104,1,104,0,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,0,10,4,10,102,1,8,29,1006,0,99,1006,0,81,1006,0,29,3,8,102,-1,8,10,1001,10,1,10,4,10,108,1,8,10,4,10,1001,8,0,59,3,8,102,-1,8,10,101,1,10,10,4,10,1008,8,1,10,4,10,102,1,8,82,1,1103,3,10,2,104,14,10,3,8,102,-1,8,10,101,1,10,10,4,10,108,1,8,10,4,10,102,1,8,111,1,108,2,10,2,1101,7,10,1,1,8,10,1,1009,5,10,3,8,1002,8,-1,10,101,1,10,10,4,10,108,0,8,10,4,10,102,1,8,149,3,8,1002,8,-1,10,101,1,10,10,4,10,1008,8,1,10,4,10,101,0,8,172,3,8,1002,8,-1,10,1001,10,1,10,4,10,108,0,8,10,4,10,1001,8,0,193,1006,0,39,2,103,4,10,2,1103,20,10,3,8,1002,8,-1,10,1001,10,1,10,4,10,1008,8,0,10,4,10,102,1,8,227,1,1106,8,10,2,109,15,10,2,106,14,10,3,8,102,-1,8,10,101,1,10,10,4,10,1008,8,1,10,4,10,101,0,8,261,3,8,102,-1,8,10,1001,10,1,10,4,10,1008,8,0,10,4,10,102,1,8,283,1,1109,9,10,2,1109,5,10,2,1,2,10,1006,0,79,101,1,9,9,1007,9,1087,10,1005,10,15,99,109,640,104,0,104,1,21101,936333124392,0,1,21101,0,335,0,1106,0,439,21102,1,824663880596,1,21102,346,1,0,1105,1,439,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,3,10,104,0,104,1,3,10,104,0,104,0,3,10,104,0,104,1,21102,1,179519553539,1,21101,393,0,0,1106,0,439,21102,46266515623,1,1,21101,0,404,0,1106,0,439,3,10,104,0,104,0,3,10,104,0,104,0,21101,0,983925826324,1,21101,0,427,0,1106,0,439,21101,988220642048,0,1,21102,1,438,0,1105,1,439,99,109,2,21201,-1,0,1,21102,1,40,2,21101,0,470,3,21101,460,0,0,1106,0,503,109,-2,2105,1,0,0,1,0,0,1,109,2,3,10,204,-1,1001,465,466,481,4,0,1001,465,1,465,108,4,465,10,1006,10,497,1101,0,0,465,109,-2,2106,0,0,0,109,4,2102,1,-1,502,1207,-3,0,10,1006,10,520,21101,0,0,-3,22102,1,-3,1,21202,-2,1,2,21102,1,1,3,21102,1,539,0,1105,1,544,109,-4,2106,0,0,109,5,1207,-3,1,10,1006,10,567,2207,-4,-2,10,1006,10,567,21202,-4,1,-4,1106,0,635,21202,-4,1,1,21201,-3,-1,2,21202,-2,2,3,21102,1,586,0,1105,1,544,21202,1,1,-4,21102,1,1,-1,2207,-4,-2,10,1006,10,605,21101,0,0,-1,22202,-2,-1,-2,2107,0,-3,10,1006,10,627,21202,-1,1,1,21102,1,627,0,105,1,502,21202,-2,-1,-2,22201,-4,-2,-4,109,-5,2106,0,0};


struct Point {
  Point() = default;
  Point(const int x, const int y) noexcept : x(x), y(y) {}

  bool operator==(const Point &rhs) const noexcept {
    return x == rhs.x &&
           y == rhs.y;
  }

  bool operator!=(const Point &rhs) const noexcept {
    return !(rhs == *this);
  }

  int x{};
  int y{};
};

struct PointHasher {
  std::size_t operator()(const Point & p) const noexcept {
    return (static_cast<std::size_t>(p.x) << 31U) + p.y;
  }
};

enum Direction {
  up,
  down,
  left,
  right
};

enum Color {
  black = 0,
  white = 1
};


struct PaintTile {
  int counter;
  Color color;
};

class PaintRobot {
public:
  explicit PaintRobot(std::unordered_map<Point, PaintTile, PointHasher> &paintedTiles) noexcept : paintedTiles(paintedTiles) {}

  void nextState(const int turn) {
    if (turn != 1 && turn != 0) {
      assert(false);
    }

    // 0 == left, 1 == right
    switch (direction) {
      case up:
        if (turn == 0) {
          direction = left;
        } else {
          direction = right;
        }
        break;
      case down:
        if (turn == 0) {
          direction = right;
        } else {
          direction = left;
        }
        break;
      case left:
        if (turn == 0) {
          direction = down;
        } else {
          direction = up;
        }
        break;
      case right:
        if (turn == 0) {
          direction = up;
        } else {
          direction = down;
        }
        break;
    }
    step();
  }

  void paint(const Color color) noexcept {
    PaintTile &tile = paintedTiles[coordinates];
    ++tile.counter;
    tile.color = color;
  }

  [[nodiscard]] const Point &getCoordinates() const {
    return coordinates;
  }

private:
  void step() noexcept {
    switch (direction) {
      case up:
        coordinates.y -= 1;
        break;
      case down:
        coordinates.y += 1;
        break;
      case left:
        coordinates.x -= 1;
        break;
      case right:
        coordinates.x += 1;
        break;
    }
  }

private:
  Point coordinates;
  Direction direction = up;
  std::unordered_map<Point, PaintTile, PointHasher> & paintedTiles;
};


void paint(std::unordered_map<Point, PaintTile, PointHasher> & colorByCoordinate) {
  PaintRobot robot(colorByCoordinate);
  auto in = [&](long & in) {
    in = colorByCoordinate[robot.getCoordinates()].color;
  };
  auto out = [&, isColor = true](long out) mutable {
    if (isColor) {
      robot.paint(static_cast<Color>(out));
      isColor = false;
    } else {
      robot.nextState(out);
      isColor = true;
    }
  };
  run_program(&input, in, out);
}

std::unordered_map<Point, PaintTile, PointHasher> paint() {
  std::unordered_map<Point, PaintTile, PointHasher> colorByCoordinate;
  paint(colorByCoordinate);
  return colorByCoordinate;
}

std::unordered_map<Point, PaintTile, PointHasher> paint_start_on_white() {
  std::unordered_map<Point, PaintTile, PointHasher> colorByCoordinate;
  colorByCoordinate[Point{0,0}].color = white;
  paint(colorByCoordinate);
  return colorByCoordinate;
}

TEST(advent_of_code, day_11) {
  const size_t expected = 2093;
  const std::unordered_map<Point, PaintTile, PointHasher> map = paint();
  size_t sum = 0;
  for (const auto &item : map) {
    if (item.second.counter > 0) {
      ++sum;
    }
  }
  EXPECT_EQ(expected, sum);
}

static char toPrintable(const Color n) noexcept {
  if (n == black) {
    return ' ';
  } else {
    return 'X';
  }
}

static void print_map(const std::vector<std::vector<Color>> & map) {
  std::cout << '\n';
  for (const auto &row : map) {
    for (const auto &point : row) {
      std::cout << std::setw(2) << toPrintable(point);
    }
    std::cout << '\n';
  }
}

struct Boarders {
  int upper = std::numeric_limits<int>::min();
  int lower = std::numeric_limits<int>::max();
  int left = std::numeric_limits<int>::max();
  int right = std::numeric_limits<int>::min();
};

Boarders find_boarders(const std::unordered_map<Point, PaintTile, PointHasher> & map) {
  Boarders boarders{};

  for (const auto &item : map) {
    const Point &location = item.first;
    const PaintTile &tile = item.second;
    if (tile.counter > 0) {
      boarders.right = std::max(location.x, boarders.right);
      boarders.left = std::min(location.x, boarders.left);
      boarders.upper = std::max(location.y, boarders.upper);
      boarders.lower = std::min(location.y, boarders.lower);
    }
  }

  return boarders;
}

TEST(advent_of_code, day_11_task_2_test_1) {
  const std::unordered_map<Point, PaintTile, PointHasher> map = paint_start_on_white();
  const Boarders boarders = find_boarders(map);
  std::vector<std::vector<Color>> canvas;
  canvas.resize(boarders.upper - boarders.lower + 1);
  for (auto &row : canvas) {
    row.resize(boarders.right - boarders.left + 1);
  }

  for (const auto &item : map) {
    const Point &location = item.first;
    const PaintTile &tile = item.second;
    if (tile.counter > 0) {
      std::vector<Color> &row = canvas.at(location.y - boarders.lower);
      row.at(location.x - boarders.left) = tile.color;
    }
  }
  print_map(canvas);
}


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}