#include <gtest/gtest.h>

#include <vector>
#include <tuple>
#include <boost/math/common_factor.hpp>

struct Vector {
  Vector() = default;
  Vector(int x, int y, int z) noexcept : x(x), y(y), z(z) {}

  Vector& operator+=(const Vector & rhs) noexcept {
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
    return *this;
  }

  bool operator==(const Vector &rhs) const noexcept {
    return x == rhs.x &&
           y == rhs.y &&
           z == rhs.z;
  }

  bool operator!=(const Vector &rhs) const noexcept {
    return !(rhs == *this);
  }

  int x{};
  int y{};
  int z{};
};

int sign(const int n) noexcept {
  if (n > 0) {
    return 1;
  } else if(n < 0) {
    return -1;
  } else {
    return 0;
  }
}

static void flatten(Vector & v) noexcept {
  v.x = sign(v.x);
  v.y = sign(v.y);
  v.z = sign(v.z);
}

struct Point {
  Point() = default;

  Point(int x, int y, int z) noexcept : x(x), y(y), z(z) {}

  bool operator==(const Point &rhs) const noexcept {
    return x == rhs.x &&
           y == rhs.y &&
           z == rhs.z;
  }

  bool operator!=(const Point &rhs) const noexcept {
    return !(rhs == *this);
  }

  Point& operator+=(const Vector & rhs) noexcept {
    x += rhs.x;
    y += rhs.y;
    z += rhs.z;
    return *this;
  }

  Vector operator-(const Point &rhs) const noexcept {
    return Vector{x - rhs.x, y - rhs.y, z - rhs.z};
  }

  int x{};
  int y{};
  int z{};
};

struct Moon {
  Moon(int x, int y, int z) noexcept : location(x,y,z) {}
  Moon(int px, int py, int pz, int vx, int vy, int vz) noexcept : location(px,py,pz), velocity(vx,vy,vz) {}

  void apply_gravity_from(const Moon & other) noexcept {
    Vector vector = other.location - location;
    flatten(vector);
    velocity += vector;
  }

  void step() noexcept {
    location += velocity;
  }

  bool operator==(const Moon &rhs) const {
    return location == rhs.location &&
           velocity == rhs.velocity;
  }

  bool operator!=(const Moon &rhs) const {
    return !(rhs == *this);
  }

  Point location;
  Vector velocity;
};

void simulate(std::vector<Moon> & moons, const int steps) noexcept {
  for (int i = 0; i < steps; ++i) {
    //apply gravity
    for (auto cur_it = moons.begin(); cur_it != moons.end(); ++cur_it) {
      for (auto it = moons.begin(); it != moons.end(); ++it) {
        if (cur_it == it) {
          continue;
        }

        Moon & cur = *cur_it;
        const Moon & other = *it;
        cur.apply_gravity_from(other);
      }
    }

    // apply velocity
    for (auto &moon : moons) {
      moon.step();
    }
  }
}

/** 
 * @return true for the axis that is on a half period mark 
 */
static std::tuple<bool, bool, bool> check_periodicity(const std::vector<Moon> &moons) noexcept {
  std::tuple<bool, bool, bool> res = {true, true, true};
  for (const auto &moon : moons) {
    const Vector &velocity = moon.velocity;
    if (velocity.x != 0) std::get<0>(res) = false;
    if (velocity.y != 0) std::get<1>(res) = false;
    if (velocity.z != 0) std::get<2>(res) = false;
  }
  return res;
}

size_t find_repetition_point(std::vector<Moon> & moons) {
  size_t counter = 0;
  size_t x_periodicity = 0;
  size_t y_periodicity = 0;
  size_t z_periodicity = 0;

  do {
    simulate(moons, 1);
    counter += 1;
    auto [x, y, z] = check_periodicity(moons);
    if (x && !x_periodicity) x_periodicity = counter*2;
    if (y && !y_periodicity) y_periodicity = counter*2;
    if (z && !z_periodicity) z_periodicity = counter*2;
    
  } while (!x_periodicity || !y_periodicity || !z_periodicity);

  const size_t periodicity = boost::math::lcm(boost::math::lcm(x_periodicity, y_periodicity), z_periodicity);
  return periodicity;
}

template <typename T>
int getEnergy(const T & t) noexcept {
  return std::abs(t.x) + std::abs(t.y) + std::abs(t.z);
}

int getTotalEnergy(const std::vector<Moon> & moons) noexcept {
  int sum = 0;
  for (const auto &moon : moons) {
    sum += (getEnergy(moon.location) * getEnergy(moon.velocity));
  }
  return sum;
}

TEST(advent_of_code, day_12_test_1) {
  std::vector<Moon> expected{{-1, 0, 2},
                             {2, -10, -7},
                             {4, -8, 8},
                             {3, 5, -1}};
  std::vector<Moon> moons{{-1, 0, 2},
                          {2, -10, -7},
                          {4, -8, 8},
                          {3, 5, -1}};
  simulate(moons, 0);

  EXPECT_EQ(expected, moons);
}

TEST(advent_of_code, day_12_test_2) {
  std::vector<Moon> expected{{2, -1, 1, 3, -1, -1},
                             {3, -7, -4, 1, 3, 3},
                             {1, -7, 5, -3, 1, -3},
                             {2, 2, 0, -1, -3, 1}};
  std::vector<Moon> moons{{-1, 0, 2},
                          {2, -10, -7},
                          {4, -8, 8},
                          {3, 5, -1}};
  simulate(moons, 1);

  EXPECT_EQ(expected, moons);
}

TEST(advent_of_code, day_12_test_3) {
  std::vector<Moon> expected{{2, 1, -3, -3, -2, 1},
                             {1, -8, 0, -1, 1, 3},
                             {3, -6, 1, 3, 2, -3},
                             {2, 0, 4, 1, -1, -1}};
  std::vector<Moon> moons{{-1, 0, 2},
                          {2, -10, -7},
                          {4, -8, 8},
                          {3, 5, -1}};
  simulate(moons, 10);

  EXPECT_EQ(expected, moons);
}

TEST(advent_of_code, day_12_test_4) {
  std::vector<Moon> expected{{2, 1, -3, -3, -2, 1},
                             {1, -8, 0, -1, 1, 3},
                             {3, -6, 1, 3, 2, -3},
                             {2, 0, 4, 1, -1, -1}};
  std::vector<Moon> moons{{-1, 0, 2},
                          {2, -10, -7},
                          {4, -8, 8},
                          {3, 5, -1}};
  simulate(moons, 10);

  EXPECT_EQ(expected, moons);
  EXPECT_EQ(179, getTotalEnergy(moons));
}

TEST(advent_of_code, day_12_test_5) {
  const int expected_energy = 1940;
  std::vector<Moon> moons{{-8, -10, 0},
                          {5, 5, 10},
                          {2, -7, 3},
                          {9, -8, -3}};
  simulate(moons, 100);

  EXPECT_EQ(expected_energy, getTotalEnergy(moons));
}

TEST(advent_of_code, day_12_task2_test_1) {
  const int expected = 2772;
  std::vector<Moon> moons{{-1, 0, 2},
                          {2, -10, -7},
                          {4, -8, 8},
                          {3, 5, -1}};
  const int repetitions = find_repetition_point(moons);

  EXPECT_EQ(expected, repetitions);
}

TEST(advent_of_code, day_12_task2_test_2) {
  const size_t expected = 4686774924;
  std::vector<Moon> moons{{-8, -10, 0},
                          {5, 5, 10},
                          {2, -7, 3},
                          {9, -8, -3}};
  const size_t repetitions = find_repetition_point(moons);

  EXPECT_EQ(expected, repetitions);
}


TEST(advent_of_code, day_12) {
  const int expected_energy = 9999;
  std::vector<Moon> moons{{14, 9, 14},
                          {9, 11, 6},
                          {-6, 14, -4},
                          {4, -4, -3}};
  simulate(moons, 1000);

  EXPECT_EQ(expected_energy, getTotalEnergy(moons));
}

TEST(advent_of_code, day_12_task_2) {
  const size_t expected = 282399002133976;
  std::vector<Moon> moons{{14, 9, 14},
                          {9, 11, 6},
                          {-6, 14, -4},
                          {4, -4, -3}};
  const size_t repetitions = find_repetition_point(moons);

  EXPECT_EQ(expected, repetitions);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}