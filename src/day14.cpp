#include <gtest/gtest.h>

#include <vector>
#include <memory>
#include <charconv>

static auto input = "4 BFNQL => 9 LMCRF\n"
                    "2 XGWNS, 7 TCRNC => 5 TPZCH\n"
                    "4 RKHMQ, 1 QHRG, 5 JDSNJ => 4 XGWNS\n"
                    "6 HWTBC, 4 XGWNS => 6 CWCD\n"
                    "1 BKPZH, 2 FLZX => 9 HWFQG\n"
                    "1 GDVD, 2 HTSW => 8 CNQW\n"
                    "2 RMDG => 9 RKHMQ\n"
                    "3 RTLHZ => 3 MSKWT\n"
                    "1 QLNHG, 1 RJHCP => 3 GRDJ\n"
                    "10 DLSD, 2 SWKHJ, 15 HTSW => 1 TCRNC\n"
                    "4 SWKHJ, 24 ZHDSD, 2 DLSD => 3 CPGJ\n"
                    "1 SWKHJ => 1 THJHK\n"
                    "129 ORE => 8 KLSMQ\n"
                    "3 SLNKW, 4 RTLHZ => 4 LPVGC\n"
                    "1 SLNKW => 5 RLGFX\n"
                    "2 QHRG, 1 SGMK => 8 RJHCP\n"
                    "9 RGKCF, 7 QHRG => 6 ZHDSD\n"
                    "8 XGWNS, 1 CPGJ => 2 QLNHG\n"
                    "2 MQFJF, 7 TBVH, 7 FZXS => 2 WZMRW\n"
                    "13 ZHDSD, 11 SLNKW, 18 RJHCP => 2 CZJR\n"
                    "1 CNQW, 5 GRDJ, 3 GDVD => 4 FLZX\n"
                    "129 ORE => 4 RHSHR\n"
                    "2 HWTBC, 2 JDSNJ => 8 QPBHG\n"
                    "1 BKPZH, 8 SWKHJ => 6 WSWBV\n"
                    "8 RJHCP, 7 FRGJK => 1 GSDT\n"
                    "6 QPBHG => 4 BKPZH\n"
                    "17 PCRQV, 6 BFNQL, 9 GSDT, 10 MQDHX, 1 ZHDSD, 1 GRDJ, 14 BRGXB, 3 RTLHZ => 8 CFGK\n"
                    "8 RMDG => 6 SGMK\n"
                    "3 CZJR => 8 RTLHZ\n"
                    "3 BFRTV => 7 RGKCF\n"
                    "6 FRGJK, 8 CZJR, 4 GRDJ => 4 BRGXB\n"
                    "4 VRVGB => 7 PCRQV\n"
                    "4 TCRNC, 1 TBVH, 2 FZXS, 1 BQGM, 1 THJHK, 19 RLGFX => 2 CRJTJ\n"
                    "5 RDNJK => 6 SWKHJ\n"
                    "2 FLVC, 2 SLNKW, 30 HWTBC => 8 DLSD\n"
                    "6 TBVH, 3 ZHDSD => 5 BQGM\n"
                    "17 RLGFX => 4 SCZQN\n"
                    "8 SWKHJ => 6 FZXS\n"
                    "9 LZHZ => 3 QDCL\n"
                    "2 ZHDSD => 1 RDNJK\n"
                    "15 FZXS, 3 TPZCH => 6 MQFJF\n"
                    "12 RLGFX, 9 QPBHG, 6 HTSW => 1 BFNQL\n"
                    "150 ORE => 9 BFRTV\n"
                    "2 BFRTV, 2 KLSMQ => 2 RMDG\n"
                    "4 VFLNM, 30 RKHMQ, 4 CRJTJ, 24 CFGK, 21 SCZQN, 4 BMGBG, 9 HWFQG, 34 CWCD, 7 LPVGC, 10 QDCL, 2 WSWBV, 2 WTZX => 1 FUEL\n"
                    "6 RHSHR, 3 RGKCF, 1 QHRG => 6 JDSNJ\n"
                    "3 MQDHX, 2 XGWNS, 12 GRDJ => 9 LZHZ\n"
                    "128 ORE => 6 ZBWLC\n"
                    "9 JDSNJ, 7 RMDG => 8 FLVC\n"
                    "4 DLSD, 12 CZJR, 3 MSKWT => 4 MQDHX\n"
                    "2 BXNX, 4 ZBWLC => 3 QHRG\n"
                    "19 LMCRF, 3 JDSNJ => 2 BMGBG\n"
                    "1 RJHCP, 26 SGMK => 9 HTSW\n"
                    "2 QPBHG => 8 VFLNM\n"
                    "2 RGKCF => 9 SLNKW\n"
                    "3 LZHZ, 2 GRDJ => 2 TBVH\n"
                    "100 ORE => 2 BXNX\n"
                    "4 DLSD, 21 JDSNJ => 8 GDVD\n"
                    "2 QHRG => 2 HWTBC\n"
                    "1 LPVGC, 8 XGWNS => 8 FRGJK\n"
                    "9 FZXS => 7 VRVGB\n"
                    "7 WZMRW, 1 TBVH, 1 VFLNM, 8 CNQW, 15 LZHZ, 25 PCRQV, 2 BRGXB => 4 WTZX";

struct Node;

struct Formula {
  int resulting_quantity = -1;
  std::vector<std::pair<int, std::string>> requirements;
};

struct Node {
  long currently_produced = 0;
  long requirement = 0;
  Formula formula;
  std::string name;
};

std::pair<int, std::string> parse_formula_unit(const std::string & i_formula) {
  std::pair<int, std::string> res;
  const unsigned long idx = i_formula.find(' ');
  if (idx == std::string::npos) {
    assert(false);
  }

  auto char_res = std::from_chars(i_formula.c_str(), i_formula.c_str() + idx, res.first);
  if (char_res.ec != std::errc()) {
    std::cerr << "failed to parse " << i_formula << '\n';
    abort();
  }
  
  res.second = i_formula.substr(idx + 1);
  return res;
}

Formula parse_formula(const std::string & i_formula) {
  static constexpr auto separator = ", ";
  Formula formula{};
  unsigned long separator_idx = i_formula.find(separator);
  // if there is no separator, then the entire string is a unit
  if(separator_idx == std::string::npos) {
    std::pair<int, std::string> pair = parse_formula_unit(i_formula);
    formula.requirements.emplace_back(std::move(pair));
  } else {
    std::pair<int, std::string> pair = parse_formula_unit(i_formula.substr(0, separator_idx));
    formula.requirements.emplace_back(std::move(pair));
    Formula rest = parse_formula(i_formula.substr(separator_idx + 2));
    formula.requirements.reserve(formula.requirements.size() + rest.requirements.size());
    std::move(rest.requirements.begin(), rest.requirements.end(), std::back_inserter(formula.requirements));
  }

  return formula;
}

std::unordered_map<std::string, Node> parse(const std::string & text) {
  std::unordered_map<std::string, Node> res;
  std::stringstream ss(text);
  for (std::string line; std::getline(ss, line);) {
    const unsigned long idx = line.find(" => ");
    if (idx == std::string::npos) {
      assert(false);
    }

    std::string s_formula = line.substr(0, idx);
    std::string s_result = line.substr(idx + 4);

    Node node;
    node.formula = parse_formula(s_formula);
    std::pair<int, std::string> pair = parse_formula_unit(s_result);
    node.name = std::move(pair.second);
    node.formula.resulting_quantity = pair.first;
    res.emplace(std::make_pair(node.name, std::move(node)));
  }

  {
    Node ore;
    ore.name = "ORE";
    Formula formula{};
//    formula.requirements.emplace_back(1, "ORE");
    formula.resulting_quantity = 1;
    ore.formula = std::move(formula);
    res[ore.name] = std::move(ore);
  }
  return res;
}

void calculate_ore_requirement(std::unordered_map<std::string, Node> & map, Node * const root) {
  const Formula &formula = root->formula;
  const long lack = root->requirement - root->currently_produced;
  if (lack <= 0) {
    return;
  }

  long formulaLoads = lack / formula.resulting_quantity;
  const long rest = lack % formula.resulting_quantity;
  if (rest != 0) {
    formulaLoads += 1;
  }

  // increase requirements
  for (const auto &item : formula.requirements) {
    map[item.second].requirement += formulaLoads*item.first;
  }

  // increase production for the requirements
  root->currently_produced += formulaLoads * formula.resulting_quantity;
  for (auto &item : formula.requirements) {
    calculate_ore_requirement(map, &map[item.second]);
  }
}

long calculate_ore_requirement(std::unordered_map<std::string, Node> & map) {
  Node & ore = map["ORE"];
  ore.currently_produced = std::numeric_limits<int>::max();
  calculate_ore_requirement(map, &map["FUEL"]);
  return ore.requirement;
}

long find_maximum_fuel(const long ore, const std::unordered_map<std::string, Node> & map) {
  long fuel;
  long upper_limit = ore + 1;
  long lower_limit = 0;
  fuel = (upper_limit - lower_limit) /2;
  for (;;) {
    std::unordered_map<std::string, Node> working_memory = map;
    working_memory["FUEL"].requirement = fuel;
    long required_ore = calculate_ore_requirement(working_memory);
    const long diff = (upper_limit - lower_limit) / 2;
    if (required_ore > ore) {
      upper_limit = fuel;
      fuel -= std::max(1L, diff);
    } else if (required_ore < ore) {
      lower_limit = fuel;
      if (diff == 0) {
        break;
      }
      fuel += diff;
    } else {
      break;
    }
  }
  return fuel;
}

class long_iterator {
public:
  using iterator_category = std::random_access_iterator_tag;

  /// The type "pointed to" by the iterator.
  using value_type = long;

  /// Distance between iterators is represented as this type.
  using difference_type = value_type;

  /// This type represents a pointer-to-value_type.
  using pointer = value_type*;

  /// This type represents a reference-to-value_type.
  using reference = value_type&;

  explicit long_iterator(value_type val) : m_val(val) {}

  long_iterator& operator++() noexcept {
    increment(1);
    return *this;
  }

  long_iterator operator++(int) & noexcept {
    long_iterator ret(*this);
    increment(1);
    return ret;
  }

  long_iterator operator--(int) & noexcept {
    long_iterator ret(*this);
    decrement(1);
    return ret;
  }

  long_iterator& operator--() noexcept {
    decrement(1);
    return *this;
  }

  long_iterator& operator+=(const difference_type n) noexcept {
    increment(n);
    return *this;
  }

  long_iterator& operator-=(const difference_type n) noexcept {
    decrement(n);
    return *this;
  }

  bool operator==(const long_iterator & rhs) const noexcept {
    return m_val == rhs.m_val;
  }

  bool operator!=(const long_iterator & rhs) const noexcept {
    return !(*this == rhs);
  }

  // this iterator does not return a reference since it wouldn't make sense to allow modification of the value
  value_type operator*() const noexcept {
    return m_val;
  }

  difference_type operator-(const long_iterator & rhs) const noexcept{
    return m_val - rhs.m_val;
  }

private:
  void increment(const difference_type n) noexcept {
    m_val += n;
  }

  void decrement(const difference_type n) noexcept {
    m_val -= n;
  }

  long m_val;
};

long find_maximum_fuel_2(const long ore, const std::unordered_map<std::string, Node> & map) {
  auto it = std::upper_bound(long_iterator(0), long_iterator(ore + 1), ore, [&](const long & value, const long & b) {
    std::unordered_map<std::string, Node> working_memory = map;
    working_memory["FUEL"].requirement = b;
    const long required_ore_b = calculate_ore_requirement(working_memory);
    return value <= required_ore_b;
  });
  // upper_bound always stops at 1 above the maximum
  return *it - 1;
}

TEST(advent_of_code, day_14_test_1) {
  const long expected = 31;
  auto test_input = "10 ORE => 10 A\n"
                    "1 ORE => 1 B\n"
                    "7 A, 1 B => 1 C\n"
                    "7 A, 1 C => 1 D\n"
                    "7 A, 1 D => 1 E\n"
                    "7 A, 1 E => 1 FUEL";
  std::unordered_map<std::string, Node> map = parse(test_input);
  map["FUEL"].requirement = 1;;
  const long val = calculate_ore_requirement(map);
  EXPECT_EQ(expected, val);
}

TEST(advent_of_code, day_14_test_2) {
  const long expected = 165;
  auto test_input = "9 ORE => 2 A\n"
                    "8 ORE => 3 B\n"
                    "7 ORE => 5 C\n"
                    "3 A, 4 B => 1 AB\n"
                    "5 B, 7 C => 1 BC\n"
                    "4 C, 1 A => 1 CA\n"
                    "2 AB, 3 BC, 4 CA => 1 FUEL";
  std::unordered_map<std::string, Node> map = parse(test_input);
  map["FUEL"].requirement = 1;;
  const long val = calculate_ore_requirement(map);
  EXPECT_EQ(expected, val);
}

TEST(advent_of_code, day_14_test_3) {
  const long expected = 13312;
  auto test_input = "157 ORE => 5 NZVS\n"
                    "165 ORE => 6 DCFZ\n"
                    "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL\n"
                    "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ\n"
                    "179 ORE => 7 PSHF\n"
                    "177 ORE => 5 HKGWZ\n"
                    "7 DCFZ, 7 PSHF => 2 XJWVT\n"
                    "165 ORE => 2 GPVTF\n"
                    "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT";
  std::unordered_map<std::string, Node> map = parse(test_input);
  map["FUEL"].requirement = 1;;
  const long val = calculate_ore_requirement(map);
  EXPECT_EQ(expected, val);
}

TEST(advent_of_code, day_14_test_4) {
  const long expected = 180697;
  auto test_input = "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG\n"
                    "17 NVRVD, 3 JNWZP => 8 VPVL\n"
                    "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL\n"
                    "22 VJHF, 37 MNCFX => 5 FWMGM\n"
                    "139 ORE => 4 NVRVD\n"
                    "144 ORE => 7 JNWZP\n"
                    "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC\n"
                    "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV\n"
                    "145 ORE => 6 MNCFX\n"
                    "1 NVRVD => 8 CXFTF\n"
                    "1 VJHF, 6 MNCFX => 4 RFSQX\n"
                    "176 ORE => 6 VJHF";
  std::unordered_map<std::string, Node> map = parse(test_input);
  map["FUEL"].requirement = 1;;
  const long val = calculate_ore_requirement(map);
  EXPECT_EQ(expected, val);
}

TEST(advent_of_code, day_14_test_5) {
  const long expected = 2210736;
  auto test_input = "171 ORE => 8 CNZTR\n"
                    "7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL\n"
                    "114 ORE => 4 BHXH\n"
                    "14 VRPVC => 6 BMBT\n"
                    "6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL\n"
                    "6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT\n"
                    "15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW\n"
                    "13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW\n"
                    "5 BMBT => 4 WPTQ\n"
                    "189 ORE => 9 KTJDG\n"
                    "1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP\n"
                    "12 VRPVC, 27 CNZTR => 2 XDBXC\n"
                    "15 KTJDG, 12 BHXH => 5 XCVML\n"
                    "3 BHXH, 2 VRPVC => 7 MZWV\n"
                    "121 ORE => 7 VRPVC\n"
                    "7 XCVML => 6 RJRHP\n"
                    "5 BHXH, 4 VRPVC => 5 LTCX";
  std::unordered_map<std::string, Node> map = parse(test_input);
  map["FUEL"].requirement = 1;;
  const long val = calculate_ore_requirement(map);
  EXPECT_EQ(expected, val);
}

TEST(advent_of_code, day_14_task_2_test_1) {
  const long expected = 82892753;
  auto test_input = "157 ORE => 5 NZVS\n"
                    "165 ORE => 6 DCFZ\n"
                    "44 XJWVT, 5 KHKGT, 1 QDVJ, 29 NZVS, 9 GPVTF, 48 HKGWZ => 1 FUEL\n"
                    "12 HKGWZ, 1 GPVTF, 8 PSHF => 9 QDVJ\n"
                    "179 ORE => 7 PSHF\n"
                    "177 ORE => 5 HKGWZ\n"
                    "7 DCFZ, 7 PSHF => 2 XJWVT\n"
                    "165 ORE => 2 GPVTF\n"
                    "3 DCFZ, 7 NZVS, 5 HKGWZ, 10 PSHF => 8 KHKGT";
  std::unordered_map<std::string, Node> map = parse(test_input);
  const long val = find_maximum_fuel(1000000000000, map);
  const long val2 = find_maximum_fuel_2(1000000000000, map);
  EXPECT_EQ(expected, val);
  EXPECT_EQ(expected, val2);
}

TEST(advent_of_code, day_14_task_2_test_2) {
  const long expected = 5586022;
  auto test_input = "2 VPVL, 7 FWMGM, 2 CXFTF, 11 MNCFX => 1 STKFG\n"
                    "17 NVRVD, 3 JNWZP => 8 VPVL\n"
                    "53 STKFG, 6 MNCFX, 46 VJHF, 81 HVMC, 68 CXFTF, 25 GNMV => 1 FUEL\n"
                    "22 VJHF, 37 MNCFX => 5 FWMGM\n"
                    "139 ORE => 4 NVRVD\n"
                    "144 ORE => 7 JNWZP\n"
                    "5 MNCFX, 7 RFSQX, 2 FWMGM, 2 VPVL, 19 CXFTF => 3 HVMC\n"
                    "5 VJHF, 7 MNCFX, 9 VPVL, 37 CXFTF => 6 GNMV\n"
                    "145 ORE => 6 MNCFX\n"
                    "1 NVRVD => 8 CXFTF\n"
                    "1 VJHF, 6 MNCFX => 4 RFSQX\n"
                    "176 ORE => 6 VJHF";
  std::unordered_map<std::string, Node> map = parse(test_input);
  const long val = find_maximum_fuel(1000000000000, map);
  const long val2 = find_maximum_fuel_2(1000000000000, map);
  EXPECT_EQ(expected, val);
  EXPECT_EQ(expected, val2);
}

TEST(advent_of_code, day_14_task_2_test_3) {
  const long expected = 460664;
  auto test_input = "171 ORE => 8 CNZTR\n"
                    "7 ZLQW, 3 BMBT, 9 XCVML, 26 XMNCP, 1 WPTQ, 2 MZWV, 1 RJRHP => 4 PLWSL\n"
                    "114 ORE => 4 BHXH\n"
                    "14 VRPVC => 6 BMBT\n"
                    "6 BHXH, 18 KTJDG, 12 WPTQ, 7 PLWSL, 31 FHTLT, 37 ZDVW => 1 FUEL\n"
                    "6 WPTQ, 2 BMBT, 8 ZLQW, 18 KTJDG, 1 XMNCP, 6 MZWV, 1 RJRHP => 6 FHTLT\n"
                    "15 XDBXC, 2 LTCX, 1 VRPVC => 6 ZLQW\n"
                    "13 WPTQ, 10 LTCX, 3 RJRHP, 14 XMNCP, 2 MZWV, 1 ZLQW => 1 ZDVW\n"
                    "5 BMBT => 4 WPTQ\n"
                    "189 ORE => 9 KTJDG\n"
                    "1 MZWV, 17 XDBXC, 3 XCVML => 2 XMNCP\n"
                    "12 VRPVC, 27 CNZTR => 2 XDBXC\n"
                    "15 KTJDG, 12 BHXH => 5 XCVML\n"
                    "3 BHXH, 2 VRPVC => 7 MZWV\n"
                    "121 ORE => 7 VRPVC\n"
                    "7 XCVML => 6 RJRHP\n"
                    "5 BHXH, 4 VRPVC => 5 LTCX";
  std::unordered_map<std::string, Node> map = parse(test_input);
  const long val = find_maximum_fuel(1000000000000, map);
  const long val2 = find_maximum_fuel_2(1000000000000, map);
  EXPECT_EQ(expected, val);
  EXPECT_EQ(expected, val2);
}

TEST(advent_of_code, day_14) {
  const long expected = 532506;
  std::unordered_map<std::string, Node> map = parse(input);
  map["FUEL"].requirement = 1;;
  const long val = calculate_ore_requirement(map);
  EXPECT_EQ(expected, val);
}

TEST(advent_of_code, day_14_task_2) {
  const long expected = 2595245;
  std::unordered_map<std::string, Node> map = parse(input);
  const long val = find_maximum_fuel(1000000000000, map);
  const long val2 = find_maximum_fuel_2(1000000000000, map);
  EXPECT_EQ(expected, val);
  EXPECT_EQ(expected, val2);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}