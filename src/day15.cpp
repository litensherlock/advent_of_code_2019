#include <gtest/gtest.h>

#include <vector>
#include <array>
#include <unordered_set>
#include "intcode-computer.h"
#include "r.h"

static std::vector<long> input{3,1033,1008,1033,1,1032,1005,1032,31,1008,1033,2,1032,1005,1032,58,1008,1033,3,1032,1005,1032,81,1008,1033,4,1032,1005,1032,104,99,101,0,1034,1039,1001,1036,0,1041,1001,1035,-1,1040,1008,1038,0,1043,102,-1,1043,1032,1,1037,1032,1042,1105,1,124,1002,1034,1,1039,1001,1036,0,1041,1001,1035,1,1040,1008,1038,0,1043,1,1037,1038,1042,1106,0,124,1001,1034,-1,1039,1008,1036,0,1041,101,0,1035,1040,1001,1038,0,1043,1002,1037,1,1042,1105,1,124,1001,1034,1,1039,1008,1036,0,1041,1002,1035,1,1040,101,0,1038,1043,1001,1037,0,1042,1006,1039,217,1006,1040,217,1008,1039,40,1032,1005,1032,217,1008,1040,40,1032,1005,1032,217,1008,1039,33,1032,1006,1032,165,1008,1040,33,1032,1006,1032,165,1101,2,0,1044,1106,0,224,2,1041,1043,1032,1006,1032,179,1101,0,1,1044,1106,0,224,1,1041,1043,1032,1006,1032,217,1,1042,1043,1032,1001,1032,-1,1032,1002,1032,39,1032,1,1032,1039,1032,101,-1,1032,1032,101,252,1032,211,1007,0,68,1044,1106,0,224,1101,0,0,1044,1105,1,224,1006,1044,247,1002,1039,1,1034,102,1,1040,1035,1001,1041,0,1036,1002,1043,1,1038,1001,1042,0,1037,4,1044,1105,1,0,67,55,37,80,63,12,30,78,95,7,20,63,83,54,86,58,97,11,84,24,11,77,42,78,22,54,89,52,44,28,93,30,81,60,58,78,87,60,54,59,78,96,17,82,74,85,66,41,89,96,54,40,82,17,22,89,65,96,71,55,81,34,90,11,85,44,58,83,79,93,30,76,62,80,16,73,20,43,40,73,69,39,39,15,93,39,99,8,74,33,97,84,24,50,91,5,71,34,81,76,22,98,50,93,80,36,76,16,76,43,19,71,63,41,21,99,40,75,55,27,82,80,83,54,66,75,61,86,14,10,74,38,92,31,49,97,20,98,15,71,59,96,53,86,35,60,6,73,71,59,79,10,84,69,23,82,14,7,76,99,45,19,96,92,14,63,55,71,46,71,34,74,73,22,95,89,10,24,59,69,17,42,96,12,92,94,66,1,69,91,36,90,94,13,17,33,46,20,89,90,24,12,94,92,83,42,73,43,70,83,55,17,92,66,23,74,99,1,92,82,54,71,96,1,22,78,74,94,66,78,40,87,13,87,73,74,89,26,26,70,42,79,3,9,84,72,55,98,56,27,73,74,57,85,66,76,88,55,58,30,97,40,71,76,6,10,55,71,43,36,99,46,59,34,37,84,61,85,90,62,98,18,39,46,84,23,70,93,9,71,5,71,94,5,59,40,71,26,90,12,45,57,74,5,92,86,32,99,20,92,82,22,44,88,29,41,89,7,86,81,72,76,9,94,94,3,8,94,71,12,93,6,82,91,91,20,86,86,38,85,95,42,86,85,19,57,90,17,85,6,84,17,81,42,77,63,26,59,9,24,85,22,31,35,93,64,90,4,16,91,67,83,23,43,63,75,3,88,93,52,14,84,85,36,95,12,51,79,54,1,16,72,1,76,79,88,63,95,77,6,91,86,23,92,54,91,51,82,45,14,98,89,74,47,52,82,80,65,74,44,58,90,14,98,42,91,6,50,88,29,81,96,25,1,97,62,62,73,61,48,82,76,93,98,49,14,74,6,97,30,47,73,77,8,89,10,17,65,21,74,95,43,83,89,72,96,27,59,20,58,80,10,70,86,42,92,26,50,98,85,3,62,20,93,86,78,19,78,91,23,90,37,71,66,97,97,95,86,40,46,79,70,37,14,98,51,91,81,4,9,77,93,19,53,70,87,40,11,95,25,93,90,17,98,39,76,92,55,57,93,39,76,13,99,58,92,26,88,80,65,34,71,62,72,17,64,38,97,85,32,4,88,69,82,51,63,61,71,77,33,90,59,74,49,76,8,76,93,55,36,71,84,7,67,47,3,85,98,9,99,32,8,79,18,28,55,77,10,30,79,77,4,1,99,82,66,90,41,64,22,82,33,20,87,24,29,80,53,72,27,17,85,84,70,16,94,11,81,92,48,85,61,47,83,21,45,92,92,38,61,75,98,52,73,80,29,82,94,29,85,61,69,59,35,84,86,60,98,63,83,69,39,10,15,64,18,85,88,63,97,95,56,13,43,75,93,13,34,85,57,37,96,39,65,60,73,73,82,11,81,80,38,88,76,23,88,19,70,2,93,46,28,79,92,91,18,6,92,96,50,77,56,45,77,36,64,83,91,64,75,48,72,71,17,69,40,82,7,6,92,70,25,23,72,9,23,84,16,17,75,76,70,60,61,99,86,21,27,85,63,80,81,55,87,93,97,53,78,53,97,14,97,49,85,65,91,72,72,5,93,34,81,10,85,86,81,19,87,61,84,11,99,96,94,8,78,13,84,9,70,0,0,21,21,1,10,1,0,0,0,0,0,0};

struct Vector {
  int x;
  int y;
};

struct Point {
  Point() = default;
  Point(const int x, const int y) noexcept : x(x), y(y) {}

  bool operator==(const Point &rhs) const noexcept {
    return x == rhs.x &&
           y == rhs.y;
  }

  bool operator!=(const Point &rhs) const noexcept {
    return !(rhs == *this);
  }

  Vector operator-(const Point &rhs) const noexcept {
    return Vector{x - rhs.x, y - rhs.y};
  }

  Point operator+(const Vector & rhs) const noexcept {
    return {x + rhs.x, y + rhs.y};
  }

  int x = 0;
  int y = 0;
};

struct PointHasher {
  std::size_t operator()(const Point & p) const noexcept {
    return (static_cast<std::size_t>(p.x) << 31U) + p.y;
  }
};

enum TileID {
  unexplored = 0,
  wall,
  droid,
  target,
  passable_space,
  oxygen
};

enum MovementStatus {
  wall_hit = 0,
  moved = 1,
  target_found = 2
};

enum movement_direction : long {
  north = 1,
  south = 2,
  east = 3,
  west = 4
};


struct Node {
  Node() = default;
  explicit Node(const Point &location) noexcept : location(location) {}

  unsigned int path_len = std::numeric_limits<unsigned int>::max();
  Point location;
  std::vector<Node*> neighbours;
};

struct NodeComparator {
  bool operator()(const Node * const a, const Node * const b) const noexcept {
    return a->path_len > b->path_len;
  }
};

void dijkstra(Node * const source, Node * const target) {
  source->path_len = 0;
  std::vector<Node*> min_queue{source};
  r::make_heap(min_queue, NodeComparator{});
  std::unordered_set<Node*> visited_set;
  while(!min_queue.empty()) {
    r::pop_heap(min_queue, NodeComparator{});
    Node * const cur = min_queue.back();
    min_queue.pop_back();

    // stop when the target is found
    if (cur == target) {
      break;
    }

    visited_set.emplace(cur);
    for (Node * const neighbour : cur->neighbours) {
      neighbour->path_len = std::min(neighbour->path_len, cur->path_len + 1);
      // add to the set if it is not visited and not already in the set
      if ((visited_set.find(neighbour) == visited_set.end()) &&
          (r::find(min_queue, neighbour) == min_queue.end())) {
        min_queue.push_back(neighbour);
        r::push_heap(min_queue, NodeComparator{});
      }
    }
  }
}

void dijkstra(Node * const source) {
  dijkstra(source, nullptr);
}

void print(const std::unordered_map<Point, TileID, PointHasher> & map);

static std::array<Point, 4> get_neighbouring_locations(const Point &cur_location) noexcept {
  return std::array<Point, 4>{Point{cur_location.x, cur_location.y - 1},
                              Point{cur_location.x, cur_location.y + 1},
                              Point{cur_location.x - 1 , cur_location.y},
                              Point{cur_location.x + 1, cur_location.y}};
}

std::unordered_map<Point, Node, PointHasher> graph_map(const std::unordered_map<Point, TileID, PointHasher> &map) {
  std::unordered_map<Point, Node, PointHasher> node_map;

  // add all passable locations to the node map
  for (const auto &item : map) {
    const Point &location = item.first;
    const TileID &tile = item.second;
    if (tile != wall) {
      auto it = node_map.find(location);
      if (it == node_map.end()) {
        node_map.emplace(std::make_pair(location, Node(location)));
      }
    }
  }

  // bind the nodes together to form the graph
  for (auto &item : node_map) {
    Node & cur_node = item.second;
    const Point & cur_location = item.first;
    for(const Point neighbour_location : get_neighbouring_locations(cur_location)) {
      auto it = node_map.find(neighbour_location);
      if (it != node_map.end()) {
        cur_node.neighbours.emplace_back(&it->second);
      }
    }
  }
  return node_map;
}

void run(std::unordered_map<Point, TileID, PointHasher> & game_map, std::vector<long> * const memory) {
  Point requested_position{};
  Point cur_position{};
  auto in = [&](long & in) -> bool {
//    print(game_map);
    bool found_close_pos = false;
    for(const Point neighbour_location : get_neighbouring_locations(cur_position)) {
      auto it = game_map.find(neighbour_location);
      if (it != game_map.end()) {
        if (it->second == TileID::unexplored) {
          requested_position = it->first;
          found_close_pos = true;
          break;
        }
      }
    }

    if (!found_close_pos) {
      std::unordered_map<Point, Node, PointHasher> graphMap = graph_map(game_map);
      dijkstra(&graphMap[cur_position]);
      auto it = r::find_if(game_map, [&graphMap](const auto & it) {
        return (it.second == TileID::unexplored) && (graphMap.find(it.first) != graphMap.end());
      });

      // done exploring everything
      if(it == game_map.end()) {
        return true;
      }

      // walk backwards from the new target to the current location to get the next step
      const Node * target = &graphMap[it->first];
      while(requested_position != target->location) {
        auto min = r::min_element(target->neighbours, [](const Node * const a, const Node * const b) {
          return a->path_len < b->path_len;
        });
        if((*min)->location != cur_position) {
          target = *min;
        } else {
          requested_position = target->location;
        }
      }
    }

    const Vector vector = requested_position - cur_position;
    if(vector.x == 1) {
      in = movement_direction::east;
    } else if(vector.x == -1) {
      in = movement_direction::west;
    } else if(vector.y == -1) {
      in = movement_direction::north;
    } else if(vector.y == 1) {
      in = movement_direction::south;
    }
    return false;
  };
  auto out = [&](const long out) {
    // reset the current tile, and move the droid to where it ended up after the switch
    auto cur_pos_it = game_map.find(cur_position);
    if (cur_pos_it->second != TileID::target)
      cur_pos_it->second = TileID::passable_space;
    switch (static_cast<MovementStatus>(out)) {
      case wall_hit:
        game_map[requested_position] = TileID::wall;
        break;
      case moved:
        game_map[requested_position] = TileID::passable_space;
        for(const auto & item : get_neighbouring_locations(requested_position)) {
          game_map.try_emplace(item, TileID::unexplored);
        }
        cur_position = requested_position;
        game_map[cur_position] = TileID::droid;
        break;
      case target_found:
        game_map[requested_position] = TileID::target;
        for(const auto & item : get_neighbouring_locations(requested_position)) {
          game_map.try_emplace(item, TileID::unexplored);
        }
        cur_position = requested_position;
        break;
    }
  };
  run_program(memory, in, out);
}

std::unordered_map<Point, TileID, PointHasher> run() {
  std::unordered_map<Point, TileID, PointHasher> gameMap;
  gameMap[Point{}] = TileID::droid;
  gameMap[Point{0, -1}] = TileID::unexplored;
  gameMap[Point{0, 1}] = TileID::unexplored;
  gameMap[Point{-1, 0}] = TileID::unexplored;
  gameMap[Point{1, 0}] = TileID::unexplored;
  run(gameMap, &input);
  return gameMap;
}

static char toPrintable(const TileID n) noexcept {
  switch (n) {
    default:
    case unexplored:
      return 'u';
    case wall:
      return 'W';
    case droid:
      return 'D';
    case target:
      return 'T';
    case passable_space:
      return '-';
    case oxygen:
      return 'O';
  }
}

static void print_map(const std::vector<std::vector<TileID>> & map) {
  std::cout << '\n';
  for (const auto &row : map) {
    for (const auto &point : row) {
      std::cout << std::setw(2) << toPrintable(point);
    }
    std::cout << '\n';
  }
}

struct Boarders {
  int upper = std::numeric_limits<int>::min();
  int lower = std::numeric_limits<int>::max();
  int left = std::numeric_limits<int>::max();
  int right = std::numeric_limits<int>::min();
};

Boarders find_boarders(const std::unordered_map<Point, TileID, PointHasher> & map) {
  Boarders boarders{};

  for (const auto &item : map) {
    const Point &location = item.first;
    boarders.right = std::max(location.x, boarders.right);
    boarders.left = std::min(location.x, boarders.left);
    boarders.upper = std::max(location.y, boarders.upper);
    boarders.lower = std::min(location.y, boarders.lower);
  }

  return boarders;
}

std::vector<std::vector<TileID>> map_to_field(const std::unordered_map<Point, TileID, PointHasher> &map) {
  const Boarders boarders = find_boarders(map);
  std::vector<std::vector<TileID>> canvas;
  canvas.resize(boarders.upper - boarders.lower + 1);
  for (auto &row : canvas) {
    row.resize(boarders.right - boarders.left + 1);
  }

  for (const auto &item : map) {
    const Point &location = item.first;
    const TileID &tile = item.second;
    std::vector<TileID> &row = canvas.at(location.y - boarders.lower);
    row.at(location.x - boarders.left) = tile;
  }
  return canvas;
}

void print(const std::unordered_map<Point, TileID, PointHasher> & map) {
  std::vector<std::vector<TileID>> canvas = map_to_field(map);
  print_map(canvas);
}

int time_to_fill_with_oxygen(std::unordered_map<Point, TileID, PointHasher> cur) {
  auto it = cur.begin();
  while(it != cur.end()) {
    if (it->second == TileID::unexplored)
      it = cur.erase(it);
    else
      ++it;
  }

  // the map now only has walls, target, and passable space
  it = r::find_if(cur, [](const std::pair<Point, TileID> & pair) {return pair.second == TileID::target;});
  it->second = TileID::oxygen;

  // the map now only has walls, oxygen, and passable space
  int count = 0;
  bool exit = false;
  while(!exit) {
//    print(cur);
    exit = true;
    std::unordered_map<Point, TileID, PointHasher> next = cur;
    for (const auto &item : cur) {
      if(item.second == TileID::oxygen) {
        for (const auto &neighbouringLocation : get_neighbouring_locations(item.first)) {
          auto nit = next.find(neighbouringLocation);
          if(nit->second == TileID::passable_space) {
            nit->second = TileID::oxygen;
            if(exit) {
              for (const auto &p : get_neighbouring_locations(neighbouringLocation)) {
                auto temp_it = next.find(p);
                if((temp_it != next.end()) && (temp_it->second == TileID::passable_space)) {
                  exit = false;
                }
              }
            }
          }
        }
      }
    }

    cur = next;
    ++count;
  }
  return count;
}

TEST(advent_of_code, day_15) {
  const long expected = 308;

  const std::unordered_map<Point, TileID, PointHasher> game_map = run();
  std::unordered_map<Point, Node, PointHasher> graphMap = graph_map(game_map);
  dijkstra(&graphMap[{}]);
  auto it = r::find_if(game_map, [](const auto & pair) {return pair.second == TileID::target;});
  const Node & target = graphMap[it->first];

  EXPECT_EQ(expected, target.path_len);
}

TEST(advent_of_code, day_15_task_2) {
  const long expected = 328;

  const std::unordered_map<Point, TileID, PointHasher> game_map = run();
  const int value = time_to_fill_with_oxygen(game_map);

  EXPECT_EQ(expected, value);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}