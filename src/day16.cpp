#include <gtest/gtest.h>

#include <vector>
#include <array>
#include <charconv>
#include "r.h"

auto input = "59756772370948995765943195844952640015210703313486295362653878290009098923609769261473534009395188480864325959786470084762607666312503091505466258796062230652769633818282653497853018108281567627899722548602257463608530331299936274116326038606007040084159138769832784921878333830514041948066594667152593945159170816779820264758715101494739244533095696039336070510975612190417391067896410262310835830006544632083421447385542256916141256383813360662952845638955872442636455511906111157861890394133454959320174572270568292972621253460895625862616228998147301670850340831993043617316938748361984714845874270986989103792418940945322846146634931990046966552";

template <typename UnderlyingIterator>
class repeating_iterator {
public:
  template <typename>
  friend class repeating_iterator;

  using iterator_category = std::forward_iterator_tag;

  /// The type "pointed to" by the iterator.
  using value_type = typename std::iterator_traits<UnderlyingIterator>::value_type;

  /// Distance between iterators is represented as this type.
  using difference_type = typename std::iterator_traits<UnderlyingIterator>::difference_type;

  /// This type represents a pointer-to-value_type.
  using pointer = typename std::iterator_traits<UnderlyingIterator>::pointer;

  /// This type represents a reference-to-value_type.
  using reference = typename std::iterator_traits<UnderlyingIterator>::reference;

  repeating_iterator(UnderlyingIterator begin, UnderlyingIterator end, const unsigned int repetitions) :
      m_counter(repetitions),
      m_repetitions(repetitions),
      m_cur(begin),
      m_begin(std::move(begin)),
      m_end(std::move(end))
  {}

  template <typename OtherIterator, typename = std::enable_if_t<std::is_convertible<OtherIterator, UnderlyingIterator>::value>>
  repeating_iterator(const repeating_iterator<OtherIterator> & rhs) :
      m_counter(rhs.m_counter),
      m_repetitions(rhs.m_repetitions),
      m_cur(rhs.m_cur),
      m_begin(rhs.m_begin),
      m_end(rhs.m_end)
  {}

  repeating_iterator& operator++() {
    increment(1);
    return *this;
  }

  repeating_iterator operator++(int) & noexcept {
    repeating_iterator ret(*this);
    increment(1);
    return ret;
  }

  repeating_iterator& operator+=(const difference_type n) noexcept {
    increment(n);
    return *this;
  }

  bool operator==(repeating_iterator rhs) const {
    return m_cur == rhs.m_cur;
  }

  bool operator!=(repeating_iterator rhs) const {
    return !operator==(rhs);
  }

  reference operator*() const {
    return *m_cur;
  }

  difference_type operator-(const repeating_iterator rhs) const {
    return m_cur - rhs.m_cur;
  }

private:
  void increment(const difference_type n) noexcept {
    for (difference_type i = 0; i < n; ++i) {
      --m_counter;
      if (m_counter == 0) {
        m_counter = m_repetitions;
        ++m_cur;
        if (m_cur == m_end) {
          m_cur = m_begin;
        }
      }
    }
  }

  unsigned short m_counter;
  const unsigned short m_repetitions;
  UnderlyingIterator m_cur;
  const UnderlyingIterator m_begin;
  const UnderlyingIterator m_end;
};

std::vector<long> to_vector(const std::string_view & in) {
  std::vector<long> cur;
  cur.reserve(in.size());
  for (const auto & c : in) {
    long value;
    auto char_res = std::from_chars(&c, &c + 1, value);
    if (char_res.ec != std::errc()) {
      std::cout << "failed to parse " <<  c << '\n';
      abort();
    }

    cur.emplace_back(value);
  }
  return cur;
}

std::string to_string(const std::vector<long> &in) {
  std::string res;
  res.reserve(in.size());
  res.append(in.size(), ' ');
  auto it = in.begin();
  for (auto & c : res) {
    auto char_res = std::to_chars(&c, &c + 1, *it);
    if (char_res.ec != std::errc()) {
      std::cout << "failed write char " <<  *it << '\n';
      abort();
    }
    ++it;
  }
  return res;
}
namespace detail {

std::vector<long> flawedFrequencyTransmissionAlgorithm(std::vector<long> cur, const int phases) {
  std::vector<long> next = cur;
  for (int i = 0; i < phases; ++i) {
    static constexpr std::array<int, 4> pattern{0, 1, 0, -1};
    for (std::vector<long>::size_type j = 0; j < next.size(); ++j) {
      repeating_iterator rit(pattern.begin(), pattern.end(), j + 1);
      ++rit; // skip the first
      next[j] = 0;
      for (std::vector<long>::size_type z = 0; z < cur.size(); ++z, ++rit) {
        const int mul = *rit;
        next[j] += cur[z] * mul;
      }
      next[j] = std::abs(next[j]);
      next[j] %= 10;
    }
    cur = next;
  }
  return cur;
}

}


std::string flawedFrequencyTransmissionAlgorithm(const std::string_view & in, const int phases) {
  std::vector<long> cur = to_vector(in);
  cur = detail::flawedFrequencyTransmissionAlgorithm(cur, phases);
  return to_string(cur);
}

std::string longFlawedFrequencyTransmissionAlgorithm(std::string_view in, const size_t repetitions, const int phases) {
  std::string long_string;
  long_string.reserve(in.size() * repetitions);

  int offset;
  auto char_res = std::from_chars(in.data(), in.data() + 7, offset);
  if (char_res.ec != std::errc()) {
    std::cout << "failed read chars " <<  in << '\n';
    abort();
  }

  for (size_t i = 0; i < repetitions; ++i) {
    long_string += in;
  }

  std::vector<long> cur = to_vector(long_string.substr(offset));

  for (int i = 0; i < phases; ++i) {
    for (int iDigit = static_cast<int>(cur.size()) - 2; iDigit >= 0; --iDigit) {
      cur[iDigit] = (cur[iDigit] + cur[iDigit + 1]) % 10;
    }
  }

  std::string res = to_string(cur);
  return res.substr(0, 8);
}

TEST(advent_of_code, day_16_test_1) {
  auto test_input = "12345678";
  const std::string expected = "48226158";
  std::string res = flawedFrequencyTransmissionAlgorithm(test_input, 1);

  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_16_test_2) {
  auto test_input = "12345678";
  const std::string expected = "01029498";
  std::string res = flawedFrequencyTransmissionAlgorithm(test_input, 4);

  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_16_test_3) {
  auto test_input = "80871224585914546619083218645595";
  const std::string expected = "24176176";
  std::string res = flawedFrequencyTransmissionAlgorithm(test_input, 100);

  EXPECT_EQ(expected, res.substr(0, 8));
}

TEST(advent_of_code, day_16_test_4) {
  auto test_input = "19617804207202209144916044189917";
  const std::string expected = "73745418";
  std::string res = flawedFrequencyTransmissionAlgorithm(test_input, 100);

  EXPECT_EQ(expected, res.substr(0, 8));
}

TEST(advent_of_code, day_16_test_5) {
  auto test_input = "69317163492948606335995924319873";
  const std::string expected = "52432133";
  std::string res = flawedFrequencyTransmissionAlgorithm(test_input, 100);

  EXPECT_EQ(expected, res.substr(0, 8));
}

TEST(advent_of_code, day_16_task_2_test_1) {
  auto test_input = "03036732577212944063491565474664";

  std::string res = longFlawedFrequencyTransmissionAlgorithm(test_input, 10000, 100);

  EXPECT_NE(std::string::npos, res.find("84462026"));
}

TEST(advent_of_code, day_16) {
  const std::string expected = "69549155";
  std::string res = flawedFrequencyTransmissionAlgorithm(input, 100);

  EXPECT_EQ(expected, res.substr(0, 8));
}


TEST(advent_of_code, day_16_task_2) {
  const std::string expected = "83253465";

  std::string res = longFlawedFrequencyTransmissionAlgorithm(input, 10000, 100);

  EXPECT_EQ(expected, res.substr(0, 8));
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}