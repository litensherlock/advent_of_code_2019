#include <gtest/gtest.h>

#include <array>
#include <cstdio>

enum instruction_status {
  OK,
  EXIT,
  ERROR
};

instruction_status execute_instruction(const size_t op_idx, std::vector<int> * const memory) {
  const int op_code = memory->at(op_idx);
  switch (op_code) {
    case 1: {
      const int param1 = memory->at(memory->at(op_idx + 1));
      const int param2 = memory->at(memory->at(op_idx + 2));
      int &res = memory->at(memory->at(op_idx + 3));
      res = param1 + param2;
      return instruction_status::OK;
    }
    case 2: {
      const int param1 = memory->at(memory->at(op_idx + 1));
      const int param2 = memory->at(memory->at(op_idx + 2));
      int &res = memory->at(memory->at(op_idx + 3));
      res = param1 * param2;
      return instruction_status::OK;
    }
    case 99: {
      return instruction_status::EXIT;
    }
    default: {
      return instruction_status::ERROR;
    }
  }
}

// assuming start at idx 0, and at least one instruction
bool run_program(std::vector<int> * const memory) {
  size_t cur_idx = 0;
  instruction_status sts;
  do {
    sts = execute_instruction(cur_idx, memory);
  } while ((sts == instruction_status::OK) && (cur_idx += 4));

  return !(sts == instruction_status::ERROR);
}




TEST(advent_of_code, day_2_task_1) {
  std::vector<int> mem{1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,5,19,23,1,23,5,27,2,27,10,31,1,5,31,35,2,35,6,39,1,6,39,43,2,13,43,47,2,9,47,51,1,6,51,55,1,55,9,59,2,6,59,63,1,5,63,67,2,67,13,71,1,9,71,75,1,75,9,79,2,79,10,83,1,6,83,87,1,5,87,91,1,6,91,95,1,95,13,99,1,10,99,103,2,6,103,107,1,107,5,111,1,111,13,115,1,115,13,119,1,13,119,123,2,123,13,127,1,127,6,131,1,131,9,135,1,5,135,139,2,139,6,143,2,6,143,147,1,5,147,151,1,151,2,155,1,9,155,0,99,2,14,0,0};

  mem[1] = 12;
  mem[2] = 2;

  const bool b = run_program(&mem);
  if (!b) {
    printf("program failed\n");
  } else {
    printf("value at index 0, value = %d\n", mem.at(0));
  }
}

TEST(advent_of_code, day_2_task_2) {
  const std::vector<int> org_mem{1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,5,19,23,1,23,5,27,2,27,10,31,1,5,31,35,2,35,6,39,1,6,39,43,2,13,43,47,2,9,47,51,1,6,51,55,1,55,9,59,2,6,59,63,1,5,63,67,2,67,13,71,1,9,71,75,1,75,9,79,2,79,10,83,1,6,83,87,1,5,87,91,1,6,91,95,1,95,13,99,1,10,99,103,2,6,103,107,1,107,5,111,1,111,13,115,1,115,13,119,1,13,119,123,2,123,13,127,1,127,6,131,1,131,9,135,1,5,135,139,2,139,6,143,2,6,143,147,1,5,147,151,1,151,2,155,1,9,155,0,99,2,14,0,0};


  for (int a = 0; a < 100; ++a) {
    for (int b = 0; b < 100; ++b) {
      std::vector<int> mem(org_mem);
      mem[1] = a;
      mem[2] = b;

      const bool sts = run_program(&mem);
      if (sts && (mem[0] == 19690720)) {
        printf("mem[1] == %d, mem[2] == %d\n", mem[1], mem[2]);
        printf("100 * %d + %d = %d\n", mem[1], mem[2], 100 * mem[1] + mem[2]);
        goto done;
      }
    }
  }

  printf("failed to find any input to satisfy the requirements\n");
  FAIL();

  done:
  [[maybe_unused]]int x = 0;
}

TEST(advent_of_code, day_2_test_1) {
  const std::vector<int> expected = std::vector{2, 0, 0, 0, 99};

  std::vector mem{1,0,0,0,99};
  const bool sts = run_program(&mem);

  EXPECT_TRUE(sts);
  EXPECT_EQ(mem, expected);
}

TEST(advent_of_code, day_2_test_2) {
  const std::vector<int> expected = std::vector{2,3,0,6,99};

  std::vector mem{2,3,0,3,99};
  const bool sts = run_program(&mem);

  EXPECT_TRUE(sts);
  EXPECT_EQ(mem, expected);
}

TEST(advent_of_code, day_2_test_3) {
  const std::vector<int> expected = std::vector{2,4,4,5,99,9801};

  std::vector mem{2,4,4,5,99,0};
  const bool sts = run_program(&mem);

  EXPECT_TRUE(sts);
  EXPECT_EQ(mem, expected);
}

TEST(advent_of_code, day_2_test_4) {
  const std::vector<int> expected = std::vector{30,1,1,4,2,5,6,0,99};
  
  std::vector mem{1,1,1,4,99,5,6,0,99};
  const bool sts = run_program(&mem);

  EXPECT_TRUE(sts);
  EXPECT_EQ(mem, expected);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}