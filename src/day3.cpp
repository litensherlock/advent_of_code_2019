#include <gtest/gtest.h>

#include <vector>
#include <cstdio>
#include <charconv>

struct vector {
  int x;
  int y;
};

struct Point {
  bool operator==(const Point &rhs) const noexcept {
    return x == rhs.x &&
           y == rhs.y;
  }

  bool operator!=(const Point &rhs) const noexcept {
    return !(rhs == *this);
  }

  vector operator-(const Point &rhs) const noexcept {
    return vector{this->x - rhs.x, this->y - rhs.y};
  }

  int x;
  int y;
  int steps;
};

struct Path {
  using iter = std::vector<Point>::iterator;
  using const_iter = std::vector<Point>::const_iterator ;
  bool operator==(const Path &rhs) const {
    return points == rhs.points;
  }

  bool operator!=(const Path &rhs) const {
    return !(rhs == *this);
  }

  [[nodiscard]] const_iter begin() const noexcept {
    return points.begin();
  }

  [[nodiscard]] const_iter end() const noexcept {
    return points.end();
  }

  std::vector<Point> points;
};

Path parse(const std::vector<const char * > & path) {
  Path points{};
  Point prev{0,0};
  points.points.emplace_back(prev);
  int value;
  for (const char * const item : path) {
    const size_t len = strlen(item);
    auto char_res = std::from_chars(item + 1, item + len, value);
    if (char_res.ec != std::errc()) {
      printf("failed to parse %s", item);
      abort();
    }
    Point cur = prev;
    switch(*item) {
      case 'R':
        cur.x += value;
        break;
      case 'L':
        cur.x -= value;
        break;
      case 'U':
        cur.y += value;
        break;
      case 'D':
        cur.y -= value;
        break;
      default:
        abort();
    }
    cur.steps += value;
    prev = cur;
    points.points.emplace_back(cur);
  }
  return points;
}

template <typename T>
int manhattan_distance(const T & p) noexcept {
  return std::abs(p.x) + std::abs(p.y);
}

int manhattan_distance(const Point & a, const Point & b) noexcept {
  return manhattan_distance(b - a);
}

bool is_parallel(const vector & a, const vector & b) noexcept {
  return (a.x == b.x) || (a.y == b.y);
}

int sign(const int n) noexcept {
  if (n > 0) {
    return 1;
  } else if(n < 0) {
    return -1;
  } else {
    return 0;
  }
}

bool is_on_line(const Point & point, const Point & a, const vector & av) noexcept {
  const vector normalized = point - a;
  if (!is_parallel(normalized, av)) {
    return false;
  }

  if ((sign(normalized.x) != sign(av.x)) ||
      (sign(normalized.y) != sign(av.y))) {
    return false;
  }

  return manhattan_distance(normalized) < manhattan_distance(av);
}

Point intersect(Point ap, vector av, Point bp, vector bv) noexcept {
  // if the vectors are parallel, then there is no intersection
  if(is_parallel(av, bv)) {
    return {};
  }
  Point intersection{};
  if (av.x != 0) {
    intersection = {bp.x, ap.y};
  } else {
    intersection = {ap.x, bp.y};
  }

  // steps are total steps this far + steps from a to intersection + steps from b to intersection
  intersection.steps = (ap.steps + bp.steps) + (manhattan_distance(ap, intersection) + manhattan_distance(bp, intersection));

  if (is_on_line(intersection, bp, bv) && is_on_line(intersection, ap, av)) {
    return intersection;
  } else {
    return {};
  }
}

std::vector<Point> find_intersections(const Path & a, const Path & b) noexcept {
  std::vector<Point> intersections;

  auto a_it = a.begin();
  auto a_trailer = *a_it++;
  for(; a_it != a.end(); ++a_it) {
    auto a_cur = *a_it;

    auto b_it = b.begin();
    auto b_trailer = *b_it++;
    for(; b_it != b.end(); ++b_it) {
      auto b_cur = *b_it;

      const vector av = a_cur - a_trailer;
      const vector bv = b_cur - b_trailer;
      if(Point intersection = intersect(a_trailer, av, b_trailer, bv);
          intersection != Point{}) {
        if (is_on_line(intersection, b_trailer, bv)) {
          intersections.emplace_back(intersection);
        }
      }

      b_trailer = b_cur;
    }
    a_trailer = a_cur;
  }

  return intersections;
}

int getShortestDistance(const std::vector<Point> & points) noexcept {
  int min = std::numeric_limits<int>::max();
  for (const auto &point : points) {
    min = std::min(manhattan_distance(point), min);
  }
  return min;
}

int getSmallestNumberOfSteps(const std::vector<Point> & points) noexcept {
  int min = std::numeric_limits<int>::max();
  for (const auto &point : points) {
    min = std::min(point.steps, min);
  }
  return min;
}

TEST(advent_of_code, day_3_test_1) {
  Path expected;
  expected.points.emplace_back(Point{0, 0});
  expected.points.emplace_back(Point{75, 0});
  expected.points.emplace_back(Point{75, -30});
  expected.points.emplace_back(Point{75+83, -30});
  expected.points.emplace_back(Point{75+83, -30 + 83});

  const std::vector string_path{"R75","D30","R83","U83"};

  const Path path = parse(string_path);

  EXPECT_EQ(expected, path);
}

TEST(advent_of_code, intersect_test) {
  Point expected{1, 1};

  Point a{1, 0};
  Point b{0, 1};

  vector a1{0, 2};
  vector b1{2, 0};

  const Point intersection = intersect(a, a1, b, b1);
  EXPECT_EQ(expected, intersection);
}

TEST(advent_of_code, intersect_test2) {
  Point expected{100, 1};

  Point a{100, 0};
  Point b{0, 1};

  vector a1{0, 2};
  vector b1{200, 0};

  const Point intersection = intersect(a, a1, b, b1);
  EXPECT_EQ(expected, intersection);
}

TEST(advent_of_code, intersect_test_wrong_direction) {
  Point expected{};

  Point a{100, 0};
  Point b{0, 1};

  vector a1{0, -2};
  vector b1{200, 0};

  const Point intersection = intersect(a, a1, b, b1);
  EXPECT_EQ(expected, intersection);
}


TEST(advent_of_code, intersect_test_parallel) {
  Point expected{};

  Point a{100, 0};
  Point b{0, 1};

  vector a1{1, 0};
  vector b1{1, 0};

  const Point intersection = intersect(a, a1, b, b1);
  EXPECT_EQ(expected, intersection);
}

TEST(advent_of_code, day_3_test_2) {
  const std::vector<Point> expected{{3,3}, {6, 5}};
  const int expected_distance = 6;
  const std::vector string_path{"R8","U5","L5","D3"};
  const std::vector string_path_2{"U7","R6","D4","L4"};

  const Path path = parse(string_path);
  const Path path2 = parse(string_path_2);

  const std::vector<Point> intersections = find_intersections(path, path2);
  EXPECT_EQ(expected.size(), intersections.size());
  for (const auto &item : expected) {
    auto it = std::find(intersections.begin(), intersections.end(), item);
    EXPECT_NE(it, intersections.end());
  }

  EXPECT_EQ(expected_distance, getShortestDistance(intersections));
}

TEST(advent_of_code, day_3_test_3) {
  const int expected_distance = 159;
  const std::vector string_path{"R75","D30","R83","U83","L12","D49","R71","U7","L72"};
  const std::vector string_path_2{"U62","R66","U55","R34","D71","R55","D58","R83"};

  const Path path = parse(string_path);
  const Path path2 = parse(string_path_2);

  const std::vector<Point> intersections = find_intersections(path, path2);
  const int shortestDistance = getShortestDistance(intersections);
  EXPECT_EQ(expected_distance, shortestDistance);
}

TEST(advent_of_code, day_3_test_4) {
  const int expected_distance = 135;
  const std::vector string_path{"R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"};
  const std::vector string_path_2{"U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"};

  const Path path = parse(string_path);
  const Path path2 = parse(string_path_2);

  const std::vector<Point> intersections = find_intersections(path, path2);
  const int shortestDistance = getShortestDistance(intersections);
  EXPECT_EQ(expected_distance, shortestDistance);
}

TEST(advent_of_code, day_3) {
  const int expected_distance = 2129;
  const std::vector string_path{"R995","D882","R144","U180","L638","U282","L907","D326","R731","D117","R323","U529","R330","U252","R73","U173","R345","U552","R230","U682","R861","U640","L930","U590","L851","D249","R669","D878","R951","D545","L690","U392","R609","D841","R273","D465","R546","U858","L518","U567","L474","D249","L463","D390","L443","U392","L196","U418","R433","U651","R520","D450","R763","U714","R495","D716","L219","D289","L451","D594","R874","U451","R406","U662","R261","D242","R821","D951","R808","D862","L871","U133","R841","D465","R710","U300","R879","D497","R85","U173","R941","U953","R705","U972","R260","D315","L632","U182","L26","D586","R438","U275","L588","U956","L550","D576","R738","U974","R648","D880","R595","D510","L789","U455","R627","U709","R7","D486","L184","U999","L404","U329","L852","D154","L232","U398","L587","U881","R938","D40","L657","D164","R45","D917","R106","U698","L824","D426","R879","U700","R847","D891","L948","U625","R663","D814","R217","U30","R610","D781","L415","D435","L904","U815","R152","U587","R287","U141","R866","D636","L290","D114","L751","D660","R6","U383","L263","U799","R330","U96","L6","U542","L449","D361","R486","U278","L990","U329","L519","U605","R501","D559","R916","U198","L499","D174","R513","U396","L473","D565","R337","D770","R211","D10","L591","D920","R367","D748","L330","U249","L307","D645","R661","U266","R234","D403","L513","U443","L989","D1","L674","D210","L537","D872","L607","D961","R894","U632","L195","U744","L426","U531","R337","D821","R113","U436","L700","U779","R555","U891","R268","D30","R958","U411","R904","U24","R760","D958","R231","U229","L561","D134","L382","D961","L237","U676","L223","U324","R663","D186","R833","U188","R419","D349","L721","U152","L912","U490","R10","D995","L98","U47","R140","D815","R826","U730","R808","U256","R479","D322","L504","D891","L413","D848","L732","U375","L307","U7","R682","U270","L495","D248","R691","D945","L70","U220","R635","D159","R269","D15","L161","U214","R814","D3","R354","D632","R469","D36","R85","U215","L243","D183","R140","U179","R812","U180","L905","U136","L34","D937","L875"};
  const std::vector string_path_2{"L999","D22","L292","U843","R390","U678","R688","D492","L489","U488","R305","U951","L636","U725","R402","U84","L676","U171","L874","D201","R64","D743","R372","U519","R221","U986","L393","D793","R72","D184","L553","D137","L187","U487","L757","U880","L535","U887","R481","U236","L382","D195","L388","D90","R125","U414","R512","D382","R972","U935","L172","D1","R957","U593","L151","D158","R396","D42","L30","D178","R947","U977","R67","D406","R744","D64","L677","U23","R792","U864","R259","U315","R314","U17","L37","D658","L642","U135","R624","U601","L417","D949","R203","D122","R76","D493","L569","U274","L330","U933","R815","D30","L630","D43","R86","U926","L661","D491","L541","D96","R868","D565","R664","D935","L336","D152","R63","U110","L782","U14","R172","D945","L732","D870","R404","U767","L907","D558","R748","U591","R461","D153","L635","D457","R241","U478","L237","U218","R393","U468","L182","D745","L388","D360","L222","D642","L151","U560","R437","D326","R852","U525","R717","U929","L470","U621","R421","U408","L540","D176","L69","U753","L200","U251","R742","U628","R534","U542","R85","D71","R283","U905","L418","D755","L593","U335","L114","D684","L576","D645","R652","D49","R86","D991","L838","D309","L73","U847","L418","U675","R991","U463","R314","D618","L433","U173","R869","D115","L18","U233","R541","U516","L570","U340","R264","D442","L259","U276","R433","D348","R524","D353","R336","D883","R580","U157","R79","D27","L134","D161","L748","D278","R322","D581","R654","D156","L930","D293","L156","U311","R807","D618","R408","U719","R366","D632","R307","D565","R478","D620","R988","D821","R365","D581","L946","D138","L943","U69","R620","U208","L407","U188","L122","U353","L751","U565","R849","D874","R668","D794","L140","D474","R289","D773","R344","D220","L55","D385","L394","U208","R305","U736","L896","D376","R331","D855","L466","U516","L741","U124","L825","U467","L525","D911","R76","U220","L610","U102","L261","D891","L585","U397","L152","U753","R822","D252","R106","U145","L7","U524","R343","U352","L357","D399","L446","D140","L723","U46","R687","D409","R884"};

  const Path path = parse(string_path);
  const Path path2 = parse(string_path_2);

  const std::vector<Point> intersections = find_intersections(path, path2);
  const int shortestDistance = getShortestDistance(intersections);
  printf("shortest distance: %d", shortestDistance);
  EXPECT_EQ(expected_distance, shortestDistance);
}

TEST(advent_of_code, day_3_t2_test_1) {
  const std::vector<Point> expected{{3,3}, {6, 5}};
  const int expected_distance = 30;
  const std::vector string_path{"R8","U5","L5","D3"};
  const std::vector string_path_2{"U7","R6","D4","L4"};

  const Path path = parse(string_path);
  const Path path2 = parse(string_path_2);

  const std::vector<Point> intersections = find_intersections(path, path2);
  EXPECT_EQ(expected.size(), intersections.size());
  for (const auto &item : expected) {
    auto it = std::find(intersections.begin(), intersections.end(), item);
    EXPECT_NE(it, intersections.end());
  }

  EXPECT_EQ(expected_distance, getSmallestNumberOfSteps(intersections));
}

TEST(advent_of_code, day_3_t2_test_2) {
  const int expected_distance = 610;
  const std::vector string_path{"R75","D30","R83","U83","L12","D49","R71","U7","L72"};
  const std::vector string_path_2{"U62","R66","U55","R34","D71","R55","D58","R83"};

  const Path path = parse(string_path);
  const Path path2 = parse(string_path_2);

  const std::vector<Point> intersections = find_intersections(path, path2);
  const int shortestDistance = getSmallestNumberOfSteps(intersections);
  EXPECT_EQ(expected_distance, shortestDistance);
}

TEST(advent_of_code, day_3_t2_test_3) {
  const int expected_distance = 410;
  const std::vector string_path{"R98","U47","R26","D63","R33","U87","L62","D20","R33","U53","R51"};
  const std::vector string_path_2{"U98","R91","D20","R16","D67","R40","U7","R15","U6","R7"};

  const Path path = parse(string_path);
  const Path path2 = parse(string_path_2);

  const std::vector<Point> intersections = find_intersections(path, path2);
  const int shortestDistance = getSmallestNumberOfSteps(intersections);
  EXPECT_EQ(expected_distance, shortestDistance);
}

TEST(advent_of_code, day_3_task_2) {
  const int expected_distance = 134662;
  const std::vector string_path{"R995","D882","R144","U180","L638","U282","L907","D326","R731","D117","R323","U529","R330","U252","R73","U173","R345","U552","R230","U682","R861","U640","L930","U590","L851","D249","R669","D878","R951","D545","L690","U392","R609","D841","R273","D465","R546","U858","L518","U567","L474","D249","L463","D390","L443","U392","L196","U418","R433","U651","R520","D450","R763","U714","R495","D716","L219","D289","L451","D594","R874","U451","R406","U662","R261","D242","R821","D951","R808","D862","L871","U133","R841","D465","R710","U300","R879","D497","R85","U173","R941","U953","R705","U972","R260","D315","L632","U182","L26","D586","R438","U275","L588","U956","L550","D576","R738","U974","R648","D880","R595","D510","L789","U455","R627","U709","R7","D486","L184","U999","L404","U329","L852","D154","L232","U398","L587","U881","R938","D40","L657","D164","R45","D917","R106","U698","L824","D426","R879","U700","R847","D891","L948","U625","R663","D814","R217","U30","R610","D781","L415","D435","L904","U815","R152","U587","R287","U141","R866","D636","L290","D114","L751","D660","R6","U383","L263","U799","R330","U96","L6","U542","L449","D361","R486","U278","L990","U329","L519","U605","R501","D559","R916","U198","L499","D174","R513","U396","L473","D565","R337","D770","R211","D10","L591","D920","R367","D748","L330","U249","L307","D645","R661","U266","R234","D403","L513","U443","L989","D1","L674","D210","L537","D872","L607","D961","R894","U632","L195","U744","L426","U531","R337","D821","R113","U436","L700","U779","R555","U891","R268","D30","R958","U411","R904","U24","R760","D958","R231","U229","L561","D134","L382","D961","L237","U676","L223","U324","R663","D186","R833","U188","R419","D349","L721","U152","L912","U490","R10","D995","L98","U47","R140","D815","R826","U730","R808","U256","R479","D322","L504","D891","L413","D848","L732","U375","L307","U7","R682","U270","L495","D248","R691","D945","L70","U220","R635","D159","R269","D15","L161","U214","R814","D3","R354","D632","R469","D36","R85","U215","L243","D183","R140","U179","R812","U180","L905","U136","L34","D937","L875"};
  const std::vector string_path_2{"L999","D22","L292","U843","R390","U678","R688","D492","L489","U488","R305","U951","L636","U725","R402","U84","L676","U171","L874","D201","R64","D743","R372","U519","R221","U986","L393","D793","R72","D184","L553","D137","L187","U487","L757","U880","L535","U887","R481","U236","L382","D195","L388","D90","R125","U414","R512","D382","R972","U935","L172","D1","R957","U593","L151","D158","R396","D42","L30","D178","R947","U977","R67","D406","R744","D64","L677","U23","R792","U864","R259","U315","R314","U17","L37","D658","L642","U135","R624","U601","L417","D949","R203","D122","R76","D493","L569","U274","L330","U933","R815","D30","L630","D43","R86","U926","L661","D491","L541","D96","R868","D565","R664","D935","L336","D152","R63","U110","L782","U14","R172","D945","L732","D870","R404","U767","L907","D558","R748","U591","R461","D153","L635","D457","R241","U478","L237","U218","R393","U468","L182","D745","L388","D360","L222","D642","L151","U560","R437","D326","R852","U525","R717","U929","L470","U621","R421","U408","L540","D176","L69","U753","L200","U251","R742","U628","R534","U542","R85","D71","R283","U905","L418","D755","L593","U335","L114","D684","L576","D645","R652","D49","R86","D991","L838","D309","L73","U847","L418","U675","R991","U463","R314","D618","L433","U173","R869","D115","L18","U233","R541","U516","L570","U340","R264","D442","L259","U276","R433","D348","R524","D353","R336","D883","R580","U157","R79","D27","L134","D161","L748","D278","R322","D581","R654","D156","L930","D293","L156","U311","R807","D618","R408","U719","R366","D632","R307","D565","R478","D620","R988","D821","R365","D581","L946","D138","L943","U69","R620","U208","L407","U188","L122","U353","L751","U565","R849","D874","R668","D794","L140","D474","R289","D773","R344","D220","L55","D385","L394","U208","R305","U736","L896","D376","R331","D855","L466","U516","L741","U124","L825","U467","L525","D911","R76","U220","L610","U102","L261","D891","L585","U397","L152","U753","R822","D252","R106","U145","L7","U524","R343","U352","L357","D399","L446","D140","L723","U46","R687","D409","R884"};

  const Path path = parse(string_path);
  const Path path2 = parse(string_path_2);

  const std::vector<Point> intersections = find_intersections(path, path2);
  const int shortestDistance = getSmallestNumberOfSteps(intersections);
  EXPECT_EQ(expected_distance, shortestDistance);
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}