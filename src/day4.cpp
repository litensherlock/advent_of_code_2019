#include <gtest/gtest.h>

#include <vector>

int increment(std::array<uint8_t, 6> & number, const int idx) noexcept {
  if (idx < 0) {
    return 10;
  }
  
  if (number[idx] == 9) {
    int floor = increment(number, idx - 1);
    if (floor != 10) {
      number[idx] = floor;
    }
  } else {
    number[idx] += 1;
  }
  return number[idx];
}

void increment(std::array<uint8_t, 6> & number) noexcept {
  bool max = true;
  for (const auto &item : number) {
    if (item != 9) {
      max = false;
    }
  }
  if (max) {
    return;
  }
  else {
    increment(number, 5);
  }
}

bool is_valid(std::array<uint8_t, 6> & number) noexcept {
  bool has_double = false;
  bool is_not_decreasing = true;
  auto it = number.begin();
  auto trailer = it++;

  for(;it != number.end(); ++it, ++trailer) {
    if(*trailer == *it) {
      has_double = true;
    }

    if (*it < *trailer) {
      is_not_decreasing = false;
    }
  }

  return has_double && is_not_decreasing;
}

bool is_valid2(std::array<uint8_t, 6> & number) noexcept {
  bool has_double = false;
  bool is_not_decreasing = true;

  uint_fast8_t digit = 0;
  uint_fast8_t count = 0;

  for(uint_fast8_t cur : number) {
    if (cur < digit) {
      is_not_decreasing = false;
    }

    if (cur == digit) {
      count += 1;
    } else {
      //first check if it ended on a streak of 2
      if (count == 2) {
        has_double = true;
      }

      digit = cur;
      count = 1;
    }
  }

  // needs to check this again in case the loop ends on a double
  if (count == 2) {
    has_double = true;
  }

  return has_double && is_not_decreasing;
}

template <typename Validator>
int numberOfPossibleAnswers(std::array<uint8_t, 6> start, std::array<uint8_t, 6> stop, Validator is_valid) noexcept {
  int counter = 0;
  for (; start <= stop; increment(start)) {
    if (is_valid(start)) {
      ++counter;
    }
  }
  return counter;
}

TEST(advent_of_code, day_4) {
  const int possibleAnswers = numberOfPossibleAnswers({2, 8, 4, 6, 3, 9}, {7, 4, 8, 7, 5, 9}, is_valid);
  EXPECT_EQ(895, possibleAnswers);
}

TEST(advent_of_code, day_4_task_2) {
  const int possibleAnswers = numberOfPossibleAnswers({2, 8, 4, 6, 3, 9}, {7, 4, 8, 7, 5, 9}, is_valid2);
  EXPECT_EQ(591, possibleAnswers);
}

TEST(advent_of_code, day_4_task_2_test) {
  const bool expected = true;
  std::array<uint8_t, 6> value{1, 1, 2, 2, 3, 3};

  EXPECT_EQ(expected, is_valid2(value));
}

TEST(advent_of_code, day_4_task_2_test_2) {
  const bool expected = false;
  std::array<uint8_t, 6> value{1, 2, 3, 4, 4, 4};

  EXPECT_EQ(expected, is_valid2(value));
}

TEST(advent_of_code, day_4_task_2_test_3) {
  const bool expected = true;
  std::array<uint8_t, 6> value{1, 1, 1, 1, 4, 4};

  EXPECT_EQ(expected, is_valid2(value));
}

TEST(advent_of_code, day_4_task_2_test_4) {
  const bool expected = true;
  std::array<uint8_t, 6> value{3, 3, 4, 4, 4, 4};

  EXPECT_EQ(expected, is_valid2(value));
}

TEST(advent_of_code, day_4_test) {
  std::array<uint8_t, 6> expected{0, 0, 0, 0, 0, 1};
  std::array<uint8_t, 6> value{0, 0, 0, 0, 0, 0};
  increment(value);
  EXPECT_EQ(expected, value);
}

TEST(advent_of_code, day_4_test_2) {
  std::array<uint8_t, 6> expected{0, 0, 0, 1, 1, 1};
  std::array<uint8_t, 6> value{0, 0, 0, 0, 9, 9};
  increment(value);
  EXPECT_EQ(expected, value);
}

TEST(advent_of_code, day_4_test_3) {
  std::array<uint8_t, 6> expected{0, 0, 9, 1, 2, 2};
  std::array<uint8_t, 6>    value{0, 0, 9, 1, 1, 9};
  increment(value);
  EXPECT_EQ(expected, value);
}

TEST(advent_of_code, day_4_test_4) {
  bool expected = true;
  std::array<uint8_t, 6> value{1, 1, 1, 1, 1, 1};
  EXPECT_EQ(expected, is_valid(value));
}

TEST(advent_of_code, day_4_test_5_decreasing) {
  bool expected = false;
  std::array<uint8_t, 6> value{2, 2, 3, 4, 5, 0};
  EXPECT_EQ(expected, is_valid(value));
}

TEST(advent_of_code, day_4_test_6_no_double) {
  bool expected = false;
  std::array<uint8_t, 6> value{1, 2, 3, 7, 8, 9};
  EXPECT_EQ(expected, is_valid(value));
}

int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}