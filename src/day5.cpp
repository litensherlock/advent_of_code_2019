#include <gtest/gtest.h>

#include <array>
#include <cstdio>

enum instruction_status {
  OK,
  EXIT,
  ERROR
};

enum instructions : int {
  add = 1,
  mul = 2,
  cin = 3,
  cout = 4,
  jump_if_true = 5,
  jump_if_false = 6,
  less_than = 7,
  equals = 8,
  quit = 99
};

enum mode_e : int {
  position = 0,
  immediate = 1
};

int op_to_instruction(const int op_code) noexcept {
  return op_code % 100;
}

template <typename OutputIterator>
void extract_modes(int mode_code, OutputIterator out) {
  const int mask = 1000;
  while (mode_code) {
    int rest = mode_code % mask;
    mode_code -= rest;
    auto mode = static_cast<mode_e>(rest / 100);
    *out = mode;
    ++out;
    mode_code /= 10;
  }
}

int get_param(const size_t offset, const mode_e addressing_mode, const std::vector<int> & memory) {
  switch (addressing_mode) {
    case mode_e::immediate: {
      return memory.at(offset);
    }
    case mode_e::position: {
      const int read_from = memory.at(offset);
      return memory.at(read_from);
    }
    default:
      std::cerr << "unknown addressing mode: " << addressing_mode << '\n';
      abort();
  }
}

template <typename In, typename Out>
instruction_status execute(std::vector<int> * const memory, In in, Out out) {
  size_t instruction_pointer = 0;
  for(;;) {
    const int op_code = memory->at(instruction_pointer);
    const int instruction = op_to_instruction(op_code);
    switch (instruction) {
      case add: {
        std::array<mode_e, 2> modes{}; // defaults to mode 0
        extract_modes(op_code - instruction, modes.begin());

        const int param1 = get_param(instruction_pointer + 1, modes[0], *memory);
        const int param2 = get_param(instruction_pointer + 2, modes[1], *memory);
        int &res = memory->at(memory->at(instruction_pointer + 3));
        res = param1 + param2;
        instruction_pointer += 4;
        break;
      }
      case mul: {
        std::array<mode_e, 2> modes{}; // defaults to mode 0
        extract_modes(op_code - instruction, modes.begin());

        const int param1 = get_param(instruction_pointer + 1, modes[0], *memory);
        const int param2 = get_param(instruction_pointer + 2, modes[1], *memory);
        int &res = memory->at(memory->at(instruction_pointer + 3));
        res = param1 * param2;
        instruction_pointer += 4;
        break;
      }
      case cin: {
        int &res = memory->at(memory->at(instruction_pointer + 1));
        in(res);
        instruction_pointer += 2;
        break;
      }
      case cout: {
        std::array<mode_e, 1> modes{}; // defaults to mode 0
        extract_modes(op_code - instruction, modes.begin());

        const int param = get_param(instruction_pointer + 1, modes[0], *memory);
        out(param);
        instruction_pointer += 2;
        break;
      }
      case jump_if_true: {
        std::array<mode_e, 2> modes{}; // defaults to mode 0
        extract_modes(op_code - instruction, modes.begin());

        const int param1 = get_param(instruction_pointer + 1, modes[0], *memory);
        const int param2 = get_param(instruction_pointer + 2, modes[1], *memory);
        instruction_pointer += 3;
        if (param1)
          instruction_pointer = param2;
        break;
      }
      case jump_if_false: {
        std::array<mode_e, 2> modes{}; // defaults to mode 0
        extract_modes(op_code - instruction, modes.begin());

        const int param1 = get_param(instruction_pointer + 1, modes[0], *memory);
        const int param2 = get_param(instruction_pointer + 2, modes[1], *memory);
        instruction_pointer += 3;
        if (!param1)
          instruction_pointer = param2;
        break;
      }
      case less_than: {
        std::array<mode_e, 2> modes{}; // defaults to mode 0
        extract_modes(op_code - instruction, modes.begin());

        const int param1 = get_param(instruction_pointer + 1, modes[0], *memory);
        const int param2 = get_param(instruction_pointer + 2, modes[1], *memory);
        int &res = memory->at(memory->at(instruction_pointer + 3));
        res = param1 < param2 ? 1 : 0;
        instruction_pointer += 4;
        break;
      }
      case equals: {
        std::array<mode_e, 2> modes{}; // defaults to mode 0
        extract_modes(op_code - instruction, modes.begin());

        const int param1 = get_param(instruction_pointer + 1, modes[0], *memory);
        const int param2 = get_param(instruction_pointer + 2, modes[1], *memory);
        int &res = memory->at(memory->at(instruction_pointer + 3));
        res = param1 == param2 ? 1 : 0;
        instruction_pointer += 4;
        break;
      }
      case quit: {
        return instruction_status::EXIT;
      }
      default: {
        return instruction_status::ERROR;
      }
    }
  }
}

// assuming start at idx 0, and at least one instruction
bool run_program(std::vector<int> * const memory) {
  const instruction_status sts = execute(memory, [](int &) {}, [](int) {});
  return !(sts == instruction_status::ERROR);
}

template <typename In, typename Out>
bool run_program(std::vector<int> * const memory, In in, Out out) {
  const instruction_status sts = execute(memory, std::move(in), std::move(out));
  return !(sts == instruction_status::ERROR);
}







TEST(advent_of_code, day_2_task_1) {
  std::vector<int> mem{1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,5,19,23,1,23,5,27,2,27,10,31,1,5,31,35,2,35,6,39,1,6,39,43,2,13,43,47,2,9,47,51,1,6,51,55,1,55,9,59,2,6,59,63,1,5,63,67,2,67,13,71,1,9,71,75,1,75,9,79,2,79,10,83,1,6,83,87,1,5,87,91,1,6,91,95,1,95,13,99,1,10,99,103,2,6,103,107,1,107,5,111,1,111,13,115,1,115,13,119,1,13,119,123,2,123,13,127,1,127,6,131,1,131,9,135,1,5,135,139,2,139,6,143,2,6,143,147,1,5,147,151,1,151,2,155,1,9,155,0,99,2,14,0,0};

  mem[1] = 12;
  mem[2] = 2;

  const bool b = run_program(&mem);
  if (!b) {
    printf("program failed\n");
  } else {
    printf("value at index 0, value = %d\n", mem.at(0));
  }
}

TEST(advent_of_code, day_2_task_2) {
  const std::vector<int> org_mem{1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,10,1,19,1,5,19,23,1,23,5,27,2,27,10,31,1,5,31,35,2,35,6,39,1,6,39,43,2,13,43,47,2,9,47,51,1,6,51,55,1,55,9,59,2,6,59,63,1,5,63,67,2,67,13,71,1,9,71,75,1,75,9,79,2,79,10,83,1,6,83,87,1,5,87,91,1,6,91,95,1,95,13,99,1,10,99,103,2,6,103,107,1,107,5,111,1,111,13,115,1,115,13,119,1,13,119,123,2,123,13,127,1,127,6,131,1,131,9,135,1,5,135,139,2,139,6,143,2,6,143,147,1,5,147,151,1,151,2,155,1,9,155,0,99,2,14,0,0};


  for (int a = 0; a < 100; ++a) {
    for (int b = 0; b < 100; ++b) {
      std::vector<int> mem(org_mem);
      mem[1] = a;
      mem[2] = b;

      const bool sts = run_program(&mem);
      if (sts && (mem[0] == 19690720)) {
        printf("mem[1] == %d, mem[2] == %d\n", mem[1], mem[2]);
        printf("100 * %d + %d = %d\n", mem[1], mem[2], 100 * mem[1] + mem[2]);
        goto done;
      }
    }
  }

  printf("failed to find any input to satisfy the requirements\n");
  FAIL();

  done:
  [[maybe_unused]]int x = 0;
}

TEST(advent_of_code, day_2_test_1) {
  const std::vector<int> expected = std::vector{2, 0, 0, 0, 99};

  std::vector mem{1,0,0,0,99};
  const bool sts = run_program(&mem);

  EXPECT_TRUE(sts);
  EXPECT_EQ(mem, expected);
}

TEST(advent_of_code, day_5_test_add_immidate) {
  const std::vector<int> expected = std::vector{1101,3,7,10,99};

  std::vector mem{1101,3,7,3,99};
  const bool sts = run_program(&mem);

  EXPECT_TRUE(sts);
  EXPECT_EQ(mem, expected);
}

TEST(advent_of_code, day_2_test_2) {
  const std::vector<int> expected = std::vector{2,3,0,6,99};

  std::vector mem{2,3,0,3,99};
  const bool sts = run_program(&mem);

  EXPECT_TRUE(sts);
  EXPECT_EQ(mem, expected);
}

TEST(advent_of_code, day_5_test_mul_immidate) {
  const std::vector<int> expected = std::vector{1102,3,7,21,99};

  std::vector mem{1102,3,7,3,99};
  const bool sts = run_program(&mem);

  EXPECT_TRUE(sts);
  EXPECT_EQ(mem, expected);
}

TEST(advent_of_code, day_2_test_3) {
  const std::vector<int> expected = std::vector{2,4,4,5,99,9801};

  std::vector mem{2,4,4,5,99,0};
  const bool sts = run_program(&mem);

  EXPECT_TRUE(sts);
  EXPECT_EQ(mem, expected);
}

TEST(advent_of_code, day_2_test_4) {
  const std::vector<int> expected = std::vector{30,1,1,4,2,5,6,0,99};
  
  std::vector mem{1,1,1,4,99,5,6,0,99};
  const bool sts = run_program(&mem);

  EXPECT_TRUE(sts);
  EXPECT_EQ(mem, expected);
}

TEST(advent_of_code, day_5_test_cout) {
  const int expected = 1337;
  int res;
  auto infun = [](int&) {};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{104, expected, 99};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_cout_2) {
  const int expected = 144;
  int res;
  auto infun = [](int&) {};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{4, 3, 99, 144};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_cin) {
  const int expected = 18;
  auto infun = [expected](int& out) {out = expected;};
  auto outfun = [](int) {};

  std::vector<int> memory = std::vector{3,1,99};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, memory[1]);
}

TEST(advent_of_code, day_5_test_equal_pos) {
  const int expected = 0;
  const int value = 0;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,9,8,9,10,9,4,9,99,-1,8};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_not_equal_pos) {
  const int expected = 1;
  const int value = 8;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,9,8,9,10,9,4,9,99,-1,8};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_less_pos) {
  const int expected = 1;
  const int value = 3;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,9,7,9,10,9,4,9,99,-1,8};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_not_less_pos) {
  const int expected = 0;
  const int value = 321;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,9,7,9,10,9,4,9,99,-1,8};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_equal_imm) {
  const int expected = 0;
  const int value = 0;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,3,1108,-1,8,3,4,3,99};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_not_equal_imm) {
  const int expected = 1;
  const int value = 8;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,3,1108,-1,8,3,4,3,99};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_less_imm) {
  const int expected = 1;
  const int value = 3;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,3,1107,-1,8,3,4,3,99};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_not_less_imm) {
  const int expected = 0;
  const int value = 321;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,3,1107,-1,8,3,4,3,99};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_jump_test_pos) {
  const int expected = 0;
  const int value = 0;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_jump_test_pos2) {
  const int expected = 1;
  const int value = 7;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}


TEST(advent_of_code, day_5_test_jump_test_imm) {
  const int expected = 0;
  const int value = 0;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,3,1105,-1,9,1101,0,0,12,4,12,99,1};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_jump_test_imm2) {
  const int expected = 1;
  const int value = 7;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,3,1105,-1,9,1101,0,0,12,4,12,99,1};
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}

TEST(advent_of_code, day_5_test_jump_test_2) {
  const int expected = 999;
  const int value = 7;
  int res;
  auto infun = [value](int& out) {out = value;};
  auto outfun = [&res](int val) {res = val;};

  std::vector<int> memory = std::vector{3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,
                                        1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,
                                        999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99
  };
  run_program(&memory, infun, outfun);
  EXPECT_EQ(expected, res);
}


TEST(advent_of_code, day_5) {
  const int expected = 6069343;
  int last_result;
  auto infun = [](int& out) {out = 1;};
  auto outfun = [&last_result](int val) {std::cout << val << '\n'; last_result = val;};

  std::vector<int> memory = std::vector{3,225,1,225,6,6,1100,1,238,225,104,0,2,218,57,224,101,-3828,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1102,26,25,224,1001,224,-650,224,4,224,1002,223,8,223,101,7,224,224,1,223,224,223,1102,44,37,225,1102,51,26,225,1102,70,94,225,1002,188,7,224,1001,224,-70,224,4,224,1002,223,8,223,1001,224,1,224,1,223,224,223,1101,86,70,225,1101,80,25,224,101,-105,224,224,4,224,102,8,223,223,101,1,224,224,1,224,223,223,101,6,91,224,1001,224,-92,224,4,224,102,8,223,223,101,6,224,224,1,224,223,223,1102,61,60,225,1001,139,81,224,101,-142,224,224,4,224,102,8,223,223,101,1,224,224,1,223,224,223,102,40,65,224,1001,224,-2800,224,4,224,1002,223,8,223,1001,224,3,224,1,224,223,223,1102,72,10,225,1101,71,21,225,1,62,192,224,1001,224,-47,224,4,224,1002,223,8,223,101,7,224,224,1,224,223,223,1101,76,87,225,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,108,226,677,224,102,2,223,223,1005,224,329,1001,223,1,223,1107,677,226,224,102,2,223,223,1006,224,344,1001,223,1,223,7,226,677,224,1002,223,2,223,1005,224,359,101,1,223,223,1007,226,226,224,102,2,223,223,1005,224,374,101,1,223,223,108,677,677,224,102,2,223,223,1006,224,389,1001,223,1,223,107,677,226,224,102,2,223,223,1006,224,404,101,1,223,223,1108,677,226,224,102,2,223,223,1006,224,419,1001,223,1,223,1107,677,677,224,1002,223,2,223,1006,224,434,101,1,223,223,1007,677,677,224,102,2,223,223,1006,224,449,1001,223,1,223,1108,226,677,224,1002,223,2,223,1006,224,464,101,1,223,223,7,677,226,224,102,2,223,223,1006,224,479,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,494,101,1,223,223,1008,226,677,224,1002,223,2,223,1005,224,509,1001,223,1,223,1007,677,226,224,102,2,223,223,1005,224,524,1001,223,1,223,8,226,226,224,102,2,223,223,1006,224,539,101,1,223,223,1108,226,226,224,1002,223,2,223,1006,224,554,101,1,223,223,107,226,226,224,1002,223,2,223,1005,224,569,1001,223,1,223,7,226,226,224,102,2,223,223,1005,224,584,101,1,223,223,1008,677,677,224,1002,223,2,223,1006,224,599,1001,223,1,223,8,226,677,224,1002,223,2,223,1006,224,614,1001,223,1,223,108,226,226,224,1002,223,2,223,1006,224,629,101,1,223,223,107,677,677,224,102,2,223,223,1005,224,644,1001,223,1,223,8,677,226,224,1002,223,2,223,1005,224,659,1001,223,1,223,1107,226,677,224,102,2,223,223,1005,224,674,1001,223,1,223,4,223,99,226};
  run_program(&memory, infun, outfun);

  EXPECT_EQ(expected, last_result);
}

TEST(advent_of_code, day_5_task2) {
  const int expected = 3188550;
  int last_result;
  auto infun = [](int& out) {out = 5;};
  auto outfun = [&last_result](int val) {std::cout << val << '\n'; last_result = val;};

  std::vector<int> memory = std::vector{3,225,1,225,6,6,1100,1,238,225,104,0,2,218,57,224,101,-3828,224,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1102,26,25,224,1001,224,-650,224,4,224,1002,223,8,223,101,7,224,224,1,223,224,223,1102,44,37,225,1102,51,26,225,1102,70,94,225,1002,188,7,224,1001,224,-70,224,4,224,1002,223,8,223,1001,224,1,224,1,223,224,223,1101,86,70,225,1101,80,25,224,101,-105,224,224,4,224,102,8,223,223,101,1,224,224,1,224,223,223,101,6,91,224,1001,224,-92,224,4,224,102,8,223,223,101,6,224,224,1,224,223,223,1102,61,60,225,1001,139,81,224,101,-142,224,224,4,224,102,8,223,223,101,1,224,224,1,223,224,223,102,40,65,224,1001,224,-2800,224,4,224,1002,223,8,223,1001,224,3,224,1,224,223,223,1102,72,10,225,1101,71,21,225,1,62,192,224,1001,224,-47,224,4,224,1002,223,8,223,101,7,224,224,1,224,223,223,1101,76,87,225,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,108,226,677,224,102,2,223,223,1005,224,329,1001,223,1,223,1107,677,226,224,102,2,223,223,1006,224,344,1001,223,1,223,7,226,677,224,1002,223,2,223,1005,224,359,101,1,223,223,1007,226,226,224,102,2,223,223,1005,224,374,101,1,223,223,108,677,677,224,102,2,223,223,1006,224,389,1001,223,1,223,107,677,226,224,102,2,223,223,1006,224,404,101,1,223,223,1108,677,226,224,102,2,223,223,1006,224,419,1001,223,1,223,1107,677,677,224,1002,223,2,223,1006,224,434,101,1,223,223,1007,677,677,224,102,2,223,223,1006,224,449,1001,223,1,223,1108,226,677,224,1002,223,2,223,1006,224,464,101,1,223,223,7,677,226,224,102,2,223,223,1006,224,479,101,1,223,223,1008,226,226,224,102,2,223,223,1006,224,494,101,1,223,223,1008,226,677,224,1002,223,2,223,1005,224,509,1001,223,1,223,1007,677,226,224,102,2,223,223,1005,224,524,1001,223,1,223,8,226,226,224,102,2,223,223,1006,224,539,101,1,223,223,1108,226,226,224,1002,223,2,223,1006,224,554,101,1,223,223,107,226,226,224,1002,223,2,223,1005,224,569,1001,223,1,223,7,226,226,224,102,2,223,223,1005,224,584,101,1,223,223,1008,677,677,224,1002,223,2,223,1006,224,599,1001,223,1,223,8,226,677,224,1002,223,2,223,1006,224,614,1001,223,1,223,108,226,226,224,1002,223,2,223,1006,224,629,101,1,223,223,107,677,677,224,102,2,223,223,1005,224,644,1001,223,1,223,8,677,226,224,1002,223,2,223,1005,224,659,1001,223,1,223,1107,226,677,224,102,2,223,223,1005,224,674,1001,223,1,223,4,223,99,226};
  run_program(&memory, infun, outfun);

  EXPECT_EQ(expected, last_result);
}


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}