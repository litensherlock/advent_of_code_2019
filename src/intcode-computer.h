#ifndef ADVENT_OF_CODE_2019__INTCODE_COMPUTER_H
#define ADVENT_OF_CODE_2019__INTCODE_COMPUTER_H

#include <vector>
#include <type_traits>

enum instruction_status {
  OK,
  EXIT,
  ERROR
};

enum instructions : int {
  add = 1,
  mul = 2,
  cin = 3,
  cout = 4,
  jump_if_true = 5,
  jump_if_false = 6,
  less_than = 7,
  equals = 8,
  adjust_rel_base = 9,
  quit = 99
};

enum mode_e : int {
  position = 0,
  immediate = 1,
  relative = 2
};

static int op_to_instruction(const int op_code) noexcept {
  return op_code % 100;
}

template <typename OutputIterator>
static void extract_modes(long mode_code, OutputIterator out) noexcept(noexcept(*out = mode_e::immediate)) {
  const long mask = 1000;
  while (mode_code) {
    long rest = mode_code % mask;
    mode_code -= rest;
    auto mode = static_cast<mode_e>(rest / 100);
    *out = mode;
    ++out;
    mode_code /= 10;
  }
}

class Computer {
public:
  using data_type = long;

  explicit Computer(std::vector<data_type> & memory) noexcept : m_memory(memory) {}

  template <typename In, typename Out>
  instruction_status execute(In in, Out out) {
    for(;;) {
      const data_type op_code = m_memory.at(m_instruction_pointer);
      const data_type instruction = op_to_instruction(op_code);
      std::array<mode_e, 3> modes{}; // defaults to mode 0
      extract_modes(op_code - instruction, modes.begin());
      switch (instruction) {
        case add: {
          const data_type param1 = get_param(m_instruction_pointer + 1, modes[0]);
          const data_type param2 = get_param(m_instruction_pointer + 2, modes[1]);
          data_type &res = get_param(m_instruction_pointer + 3, modes[2]);
          res = param1 + param2;
          m_instruction_pointer += 4;
          break;
        }
        case mul: {
          const data_type param1 = get_param(m_instruction_pointer + 1, modes[0]);
          const data_type param2 = get_param(m_instruction_pointer + 2, modes[1]);
          data_type &res = get_param(m_instruction_pointer + 3, modes[2]);
          res = param1 * param2;
          m_instruction_pointer += 4;
          break;
        }
        case cin: {
          data_type &res = get_param(m_instruction_pointer + 1, modes[0]);
          if constexpr (std::is_invocable_r_v<bool, In, long&>) {
            const bool exit = in(res);
            if(exit)
              return instruction_status::EXIT;
          } else {
            in(res);
          }
          m_instruction_pointer += 2;
          break;
        }
        case cout: {
          const data_type param = get_param(m_instruction_pointer + 1, modes[0]);
          if constexpr (std::is_invocable_r_v<bool, Out, long>) {
            const bool exit = out(param);
            if(exit)
              return instruction_status::EXIT;
          } else {
            out(param);
          }

          m_instruction_pointer += 2;
          break;
        }
        case jump_if_true: {
          const data_type param1 = get_param(m_instruction_pointer + 1, modes[0]);
          const data_type param2 = get_param(m_instruction_pointer + 2, modes[1]);
          m_instruction_pointer += 3;
          if (param1)
            m_instruction_pointer = param2;
          break;
        }
        case jump_if_false: {
          const data_type param1 = get_param(m_instruction_pointer + 1, modes[0]);
          const data_type param2 = get_param(m_instruction_pointer + 2, modes[1]);
          m_instruction_pointer += 3;
          if (!param1)
            m_instruction_pointer = param2;
          break;
        }
        case less_than: {
          const data_type param1 = get_param(m_instruction_pointer + 1, modes[0]);
          const data_type param2 = get_param(m_instruction_pointer + 2, modes[1]);
          data_type &res = get_param(m_instruction_pointer + 3, modes[2]);
          res = param1 < param2 ? 1 : 0;
          m_instruction_pointer += 4;
          break;
        }
        case equals: {
          const data_type param1 = get_param(m_instruction_pointer + 1, modes[0]);
          const data_type param2 = get_param(m_instruction_pointer + 2, modes[1]);
          data_type &res = get_param(m_instruction_pointer + 3, modes[2]);
          res = param1 == param2 ? 1 : 0;
          m_instruction_pointer += 4;
          break;
        }
        case adjust_rel_base: {
          const data_type param1 = get_param(m_instruction_pointer + 1, modes[0]);
          m_relative_base += param1;
          m_instruction_pointer += 2;
          break;
        }
        case quit: {
          return instruction_status::EXIT;
        }
        default: {
          return instruction_status::ERROR;
        }
      }
    }
  }

private:
  [[nodiscard]] data_type & get_param(const size_t offset, const mode_e addressing_mode) {
    switch (addressing_mode) {
      case mode_e::immediate: {
        return m_memory.at(offset);
      }
      case mode_e::position: {
        const data_type read_from = m_memory.at(offset);
        if (read_from >= m_memory.size())
          m_memory.resize(read_from + 1);
        return m_memory.at(read_from);
      }
      case mode_e::relative: {
        const data_type read_from = m_relative_base + m_memory.at(offset);
        if (read_from >= m_memory.size())
          m_memory.resize(read_from + 1);
        return m_memory.at(read_from);
      }
      default:
        std::cerr << "unknown addressing mode: " << addressing_mode << '\n';
        abort();
    }
  }

private:
  int m_relative_base = 0;
  size_t m_instruction_pointer = 0;
  std::vector<data_type> & m_memory;
};


bool run_program(std::vector<long> * const memory) {
  Computer comp(*memory);
  const instruction_status sts = comp.execute([](long &) {}, [](long) {});
  return !(sts == instruction_status::ERROR);
}

template <typename In, typename Out>
bool run_program(std::vector<long> * const memory, In in, Out out) {
  Computer comp(*memory);
  const instruction_status sts = comp.execute(std::move(in), std::move(out));
  return !(sts == instruction_status::ERROR);
}

#endif //ADVENT_OF_CODE_2019__INTCODE_COMPUTER_H
