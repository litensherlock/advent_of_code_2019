#ifndef ADVENT_OF_CODE_2019__PERMUTATION_H
#define ADVENT_OF_CODE_2019__PERMUTATION_H

#include <algorithm> // for std::reverse, and std::swap

/* implemented from description
 * https://en.wikipedia.org/wiki/Permutation#Generation_in_lexicographic_order
 */

template <typename Iterator>
Iterator findAklessThenAkPlusOne(Iterator begin, Iterator end) {
  Iterator idx = end;
  Iterator trailer = begin;
  for (++begin; begin != end; ++begin, ++trailer) {
    if (*trailer < *begin){
      idx = trailer;
    }
  }
  return idx;
}

template <typename BiDirectionalIt>
BiDirectionalIt findI(BiDirectionalIt begin, BiDirectionalIt end) {
  BiDirectionalIt idx = end;
  for (--end; begin != end; --end) {
    if (*begin < *end){
      return end;
    }
  }
  return idx;
}

//returns false if no more permutations are available
template <typename Iterator>
bool getPermutation(Iterator begin, Iterator end) {
  auto k = findAklessThenAkPlusOne(begin, end);

  if (k == end){
    return false;
  } else {
    auto i = findI(k, end);
    using std::swap;
    swap(*i, *k);
    std::reverse(k + 1, end);
    return true;
  }
}

template <typename Container>
bool getPermutation(Container & c) {
  using std::begin;
  using std::end;
  return getPermutation(begin(c), end(c));
}

#endif //ADVENT_OF_CODE_2019__PERMUTATION_H
